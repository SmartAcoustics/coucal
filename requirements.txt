git+ssh://git@github.com/tobygomersall/Kea.git@3143aa4dc3a5d6cf0b3c330e35f6a5792540d54f#egg=Kea
mock==4.0.3
git+ssh://git@github.com/myhdl/myhdl.git@ae25af4d593d20a26c85fbb17c0cd98a026e8595#egg=myhdl
numpy==1.22.3 git+ssh://git@github.com/tobygomersall/Ovenbird.git@b709699fedd23dc510584c8338ad178ede6b8d1e#egg=Ovenbird
git+ssh://git@github.com/tobygomersall/Veriutils.git@2b3b75ca1be88d18a6dbcafa655cfcf547bc6285#egg=Veriutils
strictyaml==1.6.1
