import random

from myhdl import Signal, block, always, always_comb, intbv, StopSimulation
from math import ceil, log

from kea.test_utils import KeaTestCase as TestCase
from kea.test_utils import KeaVivadoVHDLTestCase as VivadoVHDLTestCase
from kea.test_utils import KeaVivadoVerilogTestCase as VivadoVerilogTestCase

from coucal.test_utils import random_string_generator
from coucal.core import (
    CoreMemoryInterface, CORE_MEMORY_INTERFACE_TYPES, ADDR_BITWIDTH,
    DATA_BITWIDTH, W_STROBE_BITWIDTH)

from ._router import router
from .test_utils import random_address_map_generator
from .interfaces import PeripheralsInterface, PeripheralInterface

@block
def router_wrapper(**kwargs):

    del(kwargs['clock'])

    return router(**kwargs)

def peripherals_interface_types_generator(peripherals_interface):
    ''' Generate the types for the peripherals interface.
    '''

    peripherals_interface_types = {}

    for peripheral in peripherals_interface.peripherals:
        peripherals_interface_types[peripheral + '_rw_enable'] = 'output'
        peripherals_interface_types[peripheral + '_rw_complete'] = 'custom'
        peripherals_interface_types[peripheral + '_rw_addr'] = 'output'
        peripherals_interface_types[peripheral + '_w_data'] = 'output'
        peripherals_interface_types[peripheral + '_w_strobe'] = 'output'
        peripherals_interface_types[peripheral + '_r_data'] = 'custom'

    return peripherals_interface_types

def test_args_setup():
    ''' Generate the arguments and argument types for the DUT.
    '''

    n_peripherals = random.randrange(2, 10)
    zero_one_offset = bool(random.randrange(2))

    address_map = (
        random_address_map_generator(
            n_peripherals, zero_one_offset=zero_one_offset))

    clock = Signal(False)
    core_memory_interface = CoreMemoryInterface()
    peripherals_interface = PeripheralsInterface(address_map)

    args = {
        'clock': clock,
        'core_memory_interface': core_memory_interface,
        'peripherals_interface': peripherals_interface,
    }

    peripherals_interface_types = (
        peripherals_interface_types_generator(peripherals_interface))

    arg_types = {
        'clock': 'clock',
        'core_memory_interface': CORE_MEMORY_INTERFACE_TYPES,
        'peripherals_interface': peripherals_interface_types,
    }

    return args, arg_types

class TestRouterInterface(TestCase):
    ''' The DUT should reject incompatible interfaces and arguments.
    '''

    def setUp(self):

        self.args, _arg_types = test_args_setup()

    def test_invalid_core_memory_interface(self):
        ''' The `router` should raise an error if the `core_memory_interface`
        is not an instance of `CoreMemoryInterface`.
        '''

        self.args['core_memory_interface'] = random.randrange(0, 100)

        self.assertRaisesRegex(
            TypeError,
            ('router: core_memory_interface should be an instance of '
             'CoreMemoryInterface.'),
            router_wrapper,
            **self.args
        )

    def test_invalid_peripherals_interface(self):
        ''' The `router` should raise an error if the `peripherals_interface`
        is not an instance of `PeripheralsInterface`.
        '''

        self.args['peripherals_interface'] = random.randrange(0, 100)

        self.assertRaisesRegex(
            TypeError,
            ('router: peripherals_interface should be an instance of '
             'PeripheralsInterface.'),
            router_wrapper,
            **self.args
        )

class TestRouter(TestCase):

    def setUp(self):

        self.args, self.arg_types = test_args_setup()

        self.test_count = 0
        self.tests_run = False

    @block
    def end_tests(self, n_tests, **kwargs):

        clock = kwargs['clock']

        return_objects = []

        @always(clock.posedge)
        def control():

            if self.test_count >= n_tests:
                self.tests_run = True
                raise StopSimulation

        return_objects.append(control)

        return return_objects

    @block
    def core_memory_interface_stim(
        self, clock, core_memory_interface, lower_bounds, upper_bounds):
        ''' This block randomly drives the core_memory_interface.
        '''

        assert(isinstance(core_memory_interface, CoreMemoryInterface))
        assert(len(lower_bounds) >= 1)
        assert(len(lower_bounds) == len(upper_bounds))

        return_objects = []

        n_peripherals = len(lower_bounds)

        address_space_upper_bound = 2**len(core_memory_interface.rw_addr)

        @always(clock.posedge)
        def stim():

            core_memory_interface.rw_enable.next = bool(random.randrange(2))
            core_memory_interface.w_strobe.next = (
                random.randrange(2**len(core_memory_interface.w_strobe)))
            core_memory_interface.w_data.next = (
                random.randrange(2**len(core_memory_interface.w_data)))

            if random.random() < 0.33:
                # Update the address line on average every three cycles
                if random.random() < 0.65:
                    # For sixty five per cent of the address changes, pick a
                    # random peripheral and choose a random address in its
                    # address space.
                    n = random.choice(range(n_peripherals))
                    core_memory_interface.rw_addr.next = (
                        random.randrange(lower_bounds[n], upper_bounds[n]))

                elif random.random() < 0.1:
                    # For ten per cent of the address changes, pick either 0
                    # of the maximum address in the address space.
                    core_memory_interface.rw_addr.next = (
                        random.choice([0, address_space_upper_bound-1]))

                else:
                    # A quarter of the time use a random address which may not
                    # be valid
                    core_memory_interface.rw_addr.next = (
                        random.randrange(address_space_upper_bound))

        return_objects.append(stim)

        return return_objects

    @block
    def peripheral_interface_stim(self, clock, peripheral_interface):
        ''' This block randomly drives the peripheral_interface.
        '''

        assert(isinstance(peripheral_interface, PeripheralInterface))

        return_objects = []

        @always(clock.posedge)
        def stim():

            peripheral_interface.rw_complete.next = (
                bool(random.randrange(2)))
            peripheral_interface.r_data.next = (
                random.randrange(2**len(peripheral_interface.r_data)))

        return_objects.append(stim)

        return return_objects

    @block
    def router_check(self, **kwargs):

        clock = kwargs['clock']
        core_memory_interface = kwargs['core_memory_interface']
        peripherals_interface = kwargs['peripherals_interface']

        assert(isinstance(core_memory_interface, CoreMemoryInterface))
        assert(isinstance(peripherals_interface, PeripheralsInterface))

        return_objects = []

        core_rw_enable = core_memory_interface.rw_enable
        core_rw_complete = core_memory_interface.rw_complete
        core_rw_addr = core_memory_interface.rw_addr
        core_w_strobe = core_memory_interface.w_strobe
        core_w_data = core_memory_interface.w_data
        core_r_data = core_memory_interface.r_data

        peripheral_rw_enables = []
        peripheral_rw_completes = []
        peripheral_rw_addrs = []
        peripheral_w_datas = []
        peripheral_w_strobes = []
        peripheral_r_datas = []

        periph_lower_bounds = []
        periph_upper_bounds = []

        for peripheral in peripherals_interface.peripherals:
            # Extract the peripheral interfaces from the peripherals interface
            peripheral_interface = (
                peripherals_interface.peripheral_interface_packager(
                    peripheral))

            peripheral_rw_enables.append(peripheral_interface.rw_enable)
            peripheral_rw_completes.append(peripheral_interface.rw_complete)
            peripheral_rw_addrs.append(peripheral_interface.rw_addr)
            peripheral_w_datas.append(peripheral_interface.w_data)
            peripheral_w_strobes.append(peripheral_interface.w_strobe)
            peripheral_r_datas.append(peripheral_interface.r_data)

            periph_lower_bounds.append(peripheral_interface.offset)
            periph_upper_bounds.append(
                peripheral_interface.offset + peripheral_interface.n_bytes)

            # Create a stim block for all of the peripheral_interfaces
            return_objects.append(
                self.peripheral_interface_stim(clock, peripheral_interface))

        # Create a stim block for the core_memory_interface
        return_objects.append(
            self.core_memory_interface_stim(
                clock, core_memory_interface, periph_lower_bounds,
                periph_upper_bounds))

        n_peripherals = peripherals_interface.n_peripherals

        expected_core_rw_complete = Signal(False)
        expected_core_r_data = Signal(intbv(0)[len(core_r_data):])

        @always_comb
        def async_updates():

            expected_core_rw_complete.next = False
            expected_core_r_data.next = 0

            for n in range(n_peripherals):
                if (core_rw_addr >= periph_lower_bounds[n] and
                    core_rw_addr < periph_upper_bounds[n]):

                    expected_core_rw_complete.next = (
                        peripheral_rw_completes[n])
                    expected_core_r_data.next = peripheral_r_datas[n]

        return_objects.append(async_updates)

        @always(clock.posedge)
        def check():

            for n in range(n_peripherals):
                # Check the broadcast signals
                assert(peripheral_rw_addrs[n] == core_rw_addr)
                assert(peripheral_w_strobes[n] == core_w_strobe)
                assert(peripheral_w_datas[n] == core_w_data)

            for n in reversed(range(n_peripherals)):
                if (core_rw_addr >= periph_lower_bounds[n] and
                    core_rw_addr < periph_upper_bounds[n]):
                    assert(peripheral_rw_enables[n] == core_rw_enable)

                else:
                    assert(not peripheral_rw_enables[n])

            assert(core_rw_complete == expected_core_rw_complete)
            assert(core_r_data == expected_core_r_data)

            if core_rw_enable and core_rw_complete:
                self.test_count += 1

        return_objects.append(check)

        return return_objects

    def test_routing(self):
        ''' The `rw_addr`, `w_strobe` and `w_data` signals on the
        `core_memory_interface` should always be forwarded to all peripherals.

        The `rw_enable` signal on the `core_memory_interface` should only be
        forwarded to the peripheral specified by the `rw_addr`.

        The `rw_complete` and `r_data` signals on the `core_memory_interface`
        should be connected to the `rw_complete` and `r_data` signals from the
        peripheral specified by the `rw_addr`.
        '''

        cycles = 10000
        n_tests = 1000

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.router_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, router_wrapper, router_wrapper, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_one_peripheral(self):
        ''' The router should forward the signals correctly when the
        `peripherals_interface` contains a single peripheral.
        '''

        cycles = 10000
        n_tests = 1000

        n_peripherals = 1
        zero_one_offset = bool(random.randrange(2))
        address_map = (
            random_address_map_generator(
                n_peripherals, zero_one_offset=zero_one_offset))
        peripherals_interface = PeripheralsInterface(address_map)

        self.args['peripherals_interface'] = peripherals_interface
        self.arg_types['peripherals_interface'] = (
            peripherals_interface_types_generator(peripherals_interface))

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.router_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, router_wrapper, router_wrapper, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_peripheral_at_offset_zero(self):
        ''' The router should forward the signals correctly when the
        peripheral offset is 0.
        '''

        cycles = 10000
        n_tests = 1000

        n_peripherals = 1
        zero_one_offset = True

        address_map = (
            random_address_map_generator(
                n_peripherals, zero_one_offset=zero_one_offset))

        peripherals_interface = PeripheralsInterface(address_map)

        self.args['peripherals_interface'] = peripherals_interface
        self.arg_types['peripherals_interface'] = (
            peripherals_interface_types_generator(peripherals_interface))

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.router_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, router_wrapper, router_wrapper, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_peripheral_including_max_offset(self):
        ''' The router should forward the signals correctly when the
        peripheral address space includes the maximum possible (given the
        width of the `rw_addr` signals).
        '''

        cycles = 10000
        n_tests = 1000

        min_n_bytes_shifts = int(log(DATA_BITWIDTH//8)/log(2))
        n_bytes_options = [
            1 << n for n in range(min_n_bytes_shifts, ADDR_BITWIDTH)]

        n_bytes = random.choice(n_bytes_options)
        offset = 2**ADDR_BITWIDTH - n_bytes

        address_map = {
            random_string_generator(): {
                'offset': offset,
                'n_bytes': n_bytes}}
        peripherals_interface = PeripheralsInterface(address_map)

        self.args['peripherals_interface'] = peripherals_interface
        self.arg_types['peripherals_interface'] = (
            peripherals_interface_types_generator(peripherals_interface))

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.router_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, router_wrapper, router_wrapper, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_abutting_address_spaces(self):
        ''' The router should forward the signals correctly when the
        peripheral address spaces abut each other (when the first address
        space of one peripheral immediately follows the last address of the
        previous peripheral).
        '''

        cycles = 10000
        n_tests = 1000

        n_peripherals = random.randrange(3, 10)

        # Set n_bytes to the smallest permitted value so that we exercise all
        # of the addresses for all peripherals
        n_bytes = int(DATA_BITWIDTH/8)

        address_map = {}

        for n in range(n_peripherals):
            address_map[random_string_generator()] = {
                'offset': n*n_bytes,
                'n_bytes': n_bytes,}
        peripherals_interface = PeripheralsInterface(address_map)

        self.args['peripherals_interface'] = peripherals_interface
        self.arg_types['peripherals_interface'] = (
            peripherals_interface_types_generator(peripherals_interface))

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.router_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, router_wrapper, router_wrapper, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_min_n_bytes(self):
        ''' The router should forward the signals correctly when the
        peripheral address spaces are the minimum size permitted.

        The minimum address space size is determined by the byte width of the
        data signals (`r_data` and `w_data`). If the data signals are 4 bytes
        wide then the the peripheral must own enough addresses to fill the
        data signal.
        '''

        cycles = 10000
        n_tests = 1000

        # Set n_bytes to the smallest permitted value
        n_peripherals = random.randrange(2, 10)
        n_bytes_upper_bound = int(DATA_BITWIDTH/8) + 1
        zero_one_offset = bool(random.randrange(2))

        address_map = (
            random_address_map_generator(
                n_peripherals, n_bytes_upper_bound=n_bytes_upper_bound,
                zero_one_offset=zero_one_offset))
        peripherals_interface = PeripheralsInterface(address_map)

        self.args['peripherals_interface'] = peripherals_interface
        self.arg_types['peripherals_interface'] = (
            peripherals_interface_types_generator(peripherals_interface))

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.router_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, router_wrapper, router_wrapper, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_max_n_bytes(self):
        ''' The router should forward the signals correctly when the
        a peripheral has the maximum permitted address space.

        The maximum address space size is determined by the bitwidth of the
        address signal. The upper bound on the address space for the
        peripheral is `2**ADDR_BITWIDTH`.
        '''

        cycles = 10000
        n_tests = 1000

        # Create 1 peripheral which owns the entire address space
        address_map = {
            random_string_generator(): {
                'offset': 0,
                'n_bytes': 2**ADDR_BITWIDTH}}
        peripherals_interface = PeripheralsInterface(address_map)

        self.args['peripherals_interface'] = peripherals_interface
        self.arg_types['peripherals_interface'] = (
            peripherals_interface_types_generator(peripherals_interface))

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.router_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, router_wrapper, router_wrapper, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

class TestRouterVivadoVhdl(VivadoVHDLTestCase, TestRouter):
    pass

class TestRouterVivadoVerilog(VivadoVerilogTestCase, TestRouter):
    pass
