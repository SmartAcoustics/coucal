from myhdl import Signal, intbv

from math import log

from coucal.core import ADDR_BITWIDTH, DATA_BITWIDTH, W_STROBE_BITWIDTH

# Check that the ADDR_BITWIDTH is greater than 0 and is a power of 2
assert(ADDR_BITWIDTH > 0)
assert((ADDR_BITWIDTH & (ADDR_BITWIDTH-1)) == 0)

# Check that the DATA_BITWIDTH is at least 1 byte, is n bytes wide and is a
# power of 2.
assert(DATA_BITWIDTH >= 8)
assert(DATA_BITWIDTH % 8 == 0)
assert((DATA_BITWIDTH & (DATA_BITWIDTH-1)) == 0)

# Check that the W_STROBE_BITWIDTH is wide enough to specify every byte in the
# data signal with n hot encoding
assert(W_STROBE_BITWIDTH == DATA_BITWIDTH/8)

class PeripheralsInterface(object):

    def __init__(self, address_map):
        ''' The interfaces to the peripherals.

        The `address_map` argument should be a dictionary of the form:

            address_map = {
                "peripheral_name": {
                    "offset": x,
                    "n_bytes": y}}
        '''

        self._peripherals = list(address_map.keys())
        self._n_peripherals = len(self._peripherals)

        if self._n_peripherals <= 0:
            raise ValueError(
                'PeripheralsInterface: address_map should contain at least '
                'one entry.')

        offsets = []
        mem_upper_bounds = []

        self._data_bitwidth = DATA_BITWIDTH
        assert(self._data_bitwidth % 8 == 0)
        self._data_bytewidth = self._data_bitwidth//8

        for peripheral in self._peripherals:
            # Extract the offset and n_bytes for each peripheral
            offset = address_map[peripheral]['offset']
            n_bytes = address_map[peripheral]['n_bytes']

            if n_bytes < self._data_bytewidth:
                raise ValueError(
                    'PeripheralsInterface: All n_bytes should be greater '
                    'than the data byte width ' + hex(self._data_bytewidth) +
                    '. ' + peripheral + ' has an n_bytes of ' +
                    hex(n_bytes) + '.')

            if n_bytes % self._data_bytewidth != 0:
                raise ValueError(
                    'PeripheralsInterface: All n_bytes should be a '
                    'multiple of the data byte width ' +
                    hex(self._data_bytewidth) + '. ' + peripheral +
                    ' has an n_bytes of ' + hex(n_bytes) +
                    ' which is not a multiple of ' +
                    hex(self._data_bytewidth) + '.')

            if (n_bytes & (n_bytes-1)) != 0:
                raise ValueError(
                    'PeripheralsInterface: All n_bytes should be a power of '
                    '2. This rule means all peripherals have a unique '
                    'routing address. ' + peripheral + ' has an n_bytes of ' +
                    hex(n_bytes) + ' which is not a power of 2.')

            if offset < 0:
                raise ValueError(
                    'PeripheralsInterface: All offsets should be greater '
                    'than or equal to 0. ' + peripheral + ' has an offset of '
                    + hex(offset) + '.')

            if offset % n_bytes != 0:
                raise ValueError(
                    'PeripheralsInterface: All peripheral offsets should be '
                    'a multiple of the n bytes assigned to the peripheral. '
                    'This rule combined with all n bytes being a power of '
                    'two means that the peripheral can use n bits for '
                    'indexing in to its memory space and the router can use '
                    'the remaining bits for routing. n is calculated by '
                    'taking the log base 2 of n_bytes. ' + peripheral +
                    ' has an invalid offset.')

            if offset >= 2**ADDR_BITWIDTH:
                raise ValueError(
                    'PeripheralsInterface: All offsets should be less than ' +
                    hex(2**ADDR_BITWIDTH) + '. ' + peripheral + ' has an '
                    'offset of ' + hex(offset) + '.')

            # Calculate the exclusive upper bound for each peripheral
            upper_bound = offset + n_bytes

            if upper_bound > 2**ADDR_BITWIDTH:
                raise ValueError(
                    'PeripheralsInterface: Peripheral addresses should be '
                    'less than ' + hex(2**ADDR_BITWIDTH) + '. The '
                    'requested address range for ' + peripheral +
                    ' includes ' + hex(upper_bound-1) + '.')

            offsets.append(offset)
            mem_upper_bounds.append(upper_bound)

        for n, (o_0, u_0) in enumerate(zip(offsets, mem_upper_bounds)):
            for m, (o_1, u_1) in enumerate(
                zip(offsets[n+1:], mem_upper_bounds[n+1:])):

                # Compare each peripheral to all the others and extract the
                # overlap in their address ranges.
                overlap = range(max(o_0, o_1), min(u_0, u_1))

                # Check that there is no overlap
                if len(overlap) > 0:
                    peripheral_0 = self._peripherals[n]
                    peripheral_1 = self._peripherals[n+m+1]
                    raise ValueError(
                        'PeripheralsInterface: Peripheral addresses should '
                        'not overlap. ' + peripheral_0 + ' and ' +
                        peripheral_1 + ' are overlapping.')

        for peripheral in self._peripherals:
            # We know that the peripheral names are unique as the dictionary
            # enforces this

            # Extract the offset and n_bytes for each peripheral
            offset = address_map[peripheral]['offset']
            n_bytes = address_map[peripheral]['n_bytes']

            setattr(self, peripheral + '_offset', offset)
            setattr(self, peripheral + '_n_bytes', n_bytes)

            setattr(self, peripheral + '_rw_enable', Signal(False))
            setattr(self, peripheral + '_rw_complete', Signal(False))
            setattr(
                self, peripheral + '_rw_addr',
                Signal(intbv(0)[ADDR_BITWIDTH:0]))
            setattr(
                self, peripheral + '_w_data',
                Signal(intbv(0)[self._data_bitwidth:0]))
            setattr(
                self, peripheral + '_w_strobe',
                Signal(intbv(0)[W_STROBE_BITWIDTH:0]))
            setattr(
                self, peripheral + '_r_data',
                Signal(intbv(0)[self._data_bitwidth:0]))

    @property
    def n_peripherals(self):
        ''' Returns the number of peripherals on this interface.
        '''
        return self._n_peripherals

    @property
    def peripherals(self):
        ''' Returns a list containing the names of the peripherals on this
        interface.
        '''
        return self._peripherals

    @property
    def data_bitwidth(self):
        ''' Returns the bitwidth of the data signals on this interface.
        '''
        return self._data_bitwidth

    @property
    def data_bytewidth(self):
        ''' Returns the byte width of the data signals on this interface.
        '''
        return self._data_bytewidth

    def peripheral_interface_packager(self, peripheral):
        ''' This function packages and returns a PeripheralInterface.
        '''

        if peripheral not in self._peripherals:
            raise ValueError(
                'PeripheralsInterface: ' + peripheral  + ' is not available '
                'on this interface.')

        # Create an empty peripheral interface
        peripheral_interface = PeripheralInterface()

        # Add a reference to this PeripheralsInterface so we can get back to
        # this interface from the child peripheral interface
        peripheral_interface.parent_router_interface = self

        peripheral_interface._data_bitwidth = self._data_bitwidth
        peripheral_interface._data_bytewidth = self._data_bytewidth

        # Add the name of the peripheral
        peripheral_interface.peripheral = peripheral

        n_bytes = getattr(self, peripheral + '_n_bytes')

        # Add the offset and the n_bytes to the peripheral interface
        peripheral_interface._offset = getattr(self, peripheral + '_offset')
        peripheral_interface._n_bytes = n_bytes

        # Calculate how many addressing bits are required for n_bytes
        n_memory_addressing_bits = int(log(n_bytes)/log(2))

        # Add the n memory addressing bits and the n routing bits to the
        # peripheral interface
        peripheral_interface._n_memory_addressing_bits = (
            n_memory_addressing_bits)
        peripheral_interface._n_routing_bits = (
            ADDR_BITWIDTH - n_memory_addressing_bits)

        peripheral_interface.rw_enable = (
            getattr(self, peripheral + '_rw_enable'))
        peripheral_interface.rw_complete = (
            getattr(self, peripheral + '_rw_complete'))
        peripheral_interface.rw_addr = (
            getattr(self, peripheral + '_rw_addr'))
        peripheral_interface.w_data = (
            getattr(self, peripheral + '_w_data'))
        peripheral_interface.w_strobe = (
            getattr(self, peripheral + '_w_strobe'))
        peripheral_interface.r_data = (
            getattr(self, peripheral + '_r_data'))

        return peripheral_interface

class PeripheralInterface(object):
    ''' Note: This interface should only be created via the
    PeripheralsInterface.peripheral_interface_packager.

    Interface between the router and a peripheral.
    '''

    def __init__(self):
        pass

    def attribute_check(self, attribute):
        if not hasattr(self, attribute):
            raise AttributeError(
                'PeripheralInterface: This interface has not been set up '
                'correctly. PeripheralInterfaces should only be created by '
                'the PeripheralsInterface.peripheral_interface_packager.')

    @property
    def data_bitwidth(self):
        ''' Returns the bitwidth of the data signals on this interface.
        '''

        self.attribute_check('_data_bitwidth')

        return self._data_bitwidth

    @property
    def data_bytewidth(self):
        ''' Returns the byte width of the data signals on this interface.
        '''

        self.attribute_check('_data_bytewidth')

        return self._data_bytewidth

    @property
    def offset(self):
        ''' Returns the offset for the peripheral attached to this interface.
        '''

        self.attribute_check('_offset')

        return self._offset

    @property
    def n_bytes(self):
        ''' Returns the n_bytes for the peripheral attached to this interface.
        '''

        self.attribute_check('_n_bytes')

        return self._n_bytes

    @property
    def n_memory_addressing_bits(self):
        ''' Returns the number of bits in the rw_addr signal used for
        addressing the peripheral memory.
        '''

        self.attribute_check('_n_memory_addressing_bits')

        return self._n_memory_addressing_bits

    @property
    def n_routing_bits(self):
        ''' Returns the number of bits in the rw_addr signal used for
        routing to a specific peripheral.
        '''

        self.attribute_check('_n_routing_bits')

        return self._n_routing_bits

    @property
    def routing_bits_lsb_index(self):
        ''' Returns the bit offset of the routing bits in the rw_addr signal.
        '''

        self.attribute_check('_n_memory_addressing_bits')

        # Any bits not used for addressing the peripheral memory should be
        # used for routing.
        routing_bits_lsb_index = self._n_memory_addressing_bits

        return routing_bits_lsb_index

