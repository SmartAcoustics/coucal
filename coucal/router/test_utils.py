import random

from math import log, ceil

from coucal.test_utils import random_string_generator

from .interfaces import ADDR_BITWIDTH, DATA_BITWIDTH

def random_address_map_generator(
    n_peripherals, n_bytes_upper_bound=None, zero_one_offset=False):
    ''' This function generates a random but valid address map.
    '''

    assert(n_peripherals > 0)

    data_bytewidth = int(DATA_BITWIDTH/8)

    if n_bytes_upper_bound is not None:
        assert(n_bytes_upper_bound > data_bytewidth)

        # Define the upper bound of shifts that can produce n_bytes
        n_bytes_shifts_upper_bound = ceil(log(n_bytes_upper_bound)/log(2))

    else:
        n_bytes_shifts_upper_bound = 32 + 1

    # Offset and n_bytes must be a multiple of data_bytewidth
    min_shifts = int(log(data_bytewidth)/log(2))

    max_offset = 2**ADDR_BITWIDTH

    # The offsets must be a multiple of a power of two. Create a list of all
    # valid powers of 2 steps.
    offset_step_options = [2**n for n in range(min_shifts, ADDR_BITWIDTH)]

    offsets = []
    n_offset_conflicts = 0

    while len(offsets) < n_peripherals:
        # Select a random step for the offset range.
        step = random.choice(offset_step_options)

        # Generate a random offset
        offset = random.randrange(0, max_offset, step)

        if offset not in offsets:
            # The generated offset is not in the list already so add it.
            offsets.append(offset)

        else:
            # Count the n_offset_conflicts to make sure the while loop doesn't
            # run for ever
            n_offset_conflicts += 1
            assert(n_offset_conflicts < 1000)

    if 0 not in offsets:
        if zero_one_offset:
            # Set a random offset to zero half the time
            offsets[random.randrange(len(offsets))] = 0

    # Sort the offsets into ascending order
    offsets.sort()

    memory_regions = []

    for n, offset in enumerate(offsets):
        try:
            # Extract the next offset from the list of offsets
            next_offset = offsets[n+1]

        except IndexError:
            # The last offset in the list can access up to the max_offset
            next_offset = max_offset

        # Calculate the number of bytes available between the offset and the
        # next offset
        available_n_bytes = next_offset - offset

        # Calculate the upper bound of shifts that will keep n_bytes within
        # the available n bytes
        available_shifts_upper_bound = int(log(available_n_bytes)/log(2)) + 1

        # Calculate the number of trailing zeros on the offset. This tells us
        # the upper bound of shifts that will keep n_bytes within the range
        # supported by this offset.
        if offset == 0:
            offset_dependent_shifts_upper_bound = 32 + 1

        else:
            offset_dependent_shifts_upper_bound = (
                offset & -offset).bit_length()

        # Determine which of the shift upper bounds is the smallest so that
        # the n_shifts_upper_bound will keep n_bytes valid.
        n_shifts_upper_bound = min(
            n_bytes_shifts_upper_bound,
            available_shifts_upper_bound,
            offset_dependent_shifts_upper_bound)

        # Generate a random and valid n_bytes
        n_bytes = 1 << random.randrange(min_shifts, n_shifts_upper_bound)

        # Define the memory region
        memory_regions.append({
            'offset': offset,
            'n_bytes': n_bytes
        })

    # Shuffle the order of the memory regions so the address map doesn't
    # always contain neatly ascending memory regions
    random.shuffle(memory_regions)

    # Create the address map
    address_map = {
        random_string_generator(): region for region in memory_regions}

    return address_map
