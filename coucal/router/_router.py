from myhdl import block, always_comb, Signal, intbv

from kea.utils import constant_assigner, signal_slicer

from coucal.core import CoreMemoryInterface

from .interfaces import PeripheralsInterface

@block
def address_decoder(
    core_memory_interface, peripheral_interface, peripheral_en):
    ''' This block checks the routing bits of the
    core_memory_interface.rw_addr and sets peripheral_en high when rw_addr
    is set to an address within the address space of the peripheral_interface.
    '''

    # Sanity check the signal widths
    assert(
        len(peripheral_interface.rw_addr) ==
        len(core_memory_interface.rw_addr))

    return_objects = []

    if peripheral_interface.n_routing_bits == 0:

        # The peripheral must own the entire address space. A sanity check is
        # included to make sure.
        peripheral_n_bytes = peripheral_interface.n_bytes
        addr_space_n_bytes = 2**len(core_memory_interface.rw_addr)
        assert(peripheral_n_bytes == addr_space_n_bytes)

        # The peripheral owns the entire address space so any address is
        # intended for this peripheral.
        return_objects.append(constant_assigner(True, peripheral_en))

    else:

        # Extract the index (within the rw_addr) of the least significant bit
        # of the routing bits
        routing_bits_lsb_index = peripheral_interface.routing_bits_lsb_index

        # Determing the peripheral routing address
        offset =  peripheral_interface.offset
        peripheral_routing_addr = offset >> routing_bits_lsb_index

        # Slice the routing bits out of the rw_addr
        rw_addr = core_memory_interface.rw_addr
        n_rw_addr_routing_bits = len(rw_addr) - routing_bits_lsb_index
        rw_addr_routing_bits = Signal(intbv(0)[n_rw_addr_routing_bits:])
        return_objects.append(
            signal_slicer(
                rw_addr, routing_bits_lsb_index, n_rw_addr_routing_bits,
                rw_addr_routing_bits))

        @always_comb
        def control():

            if rw_addr_routing_bits == peripheral_routing_addr:
                # Set peripheral_en high whenever rw_addr is within the
                # address space of this peripheral.
                peripheral_en.next = True

            else:
                # Set peripheral_en low whenever the rw_addr is outside the
                # address space of this peripheral.
                peripheral_en.next = False

        return_objects.append(control)

    return return_objects

@block
def peripheral_interface_controller(
    core_memory_interface, peripheral_interface, peripheral_en):
    ''' This block always forwards the rw_addr, w_strobe and w_data to the
    peripheral interface.

    When peripheral_en is set high it also forwards the rw_enable signal.
    '''

    # Sanity check the signal widths
    assert(
        len(peripheral_interface.rw_addr) ==
        len(core_memory_interface.rw_addr))
    assert(
        len(peripheral_interface.w_strobe) ==
        len(core_memory_interface.w_strobe))
    assert(
        len(peripheral_interface.w_data) ==
        len(core_memory_interface.w_data))
    assert(
        len(peripheral_interface.r_data) ==
        len(core_memory_interface.r_data))

    return_objects = []

    @always_comb
    def control():

        # Forward the rw_addr, w_strobe and w_data signals to the peripheral
        peripheral_interface.rw_addr.next = core_memory_interface.rw_addr
        peripheral_interface.w_strobe.next = core_memory_interface.w_strobe
        peripheral_interface.w_data.next = core_memory_interface.w_data

        if peripheral_en:
            # Pass the rw_enable through to the peripheral
            peripheral_interface.rw_enable.next = (
                core_memory_interface.rw_enable)

        else:
            # Set rw_enable low whenever peripheral_en is low.
            peripheral_interface.rw_enable.next = False

    return_objects.append(control)

    return return_objects

@block
def router(core_memory_interface, peripherals_interface):
    ''' This block multiplexes the r_data and rw_complete signals from the
    peripherals onto the core memory interface.

    It also sets the correct peripheral rw_enable signal depending on the
    address.
    '''

    if not isinstance(core_memory_interface, CoreMemoryInterface):
        raise TypeError(
            'router: core_memory_interface should be an instance of '
            'CoreMemoryInterface.')

    if not isinstance(peripherals_interface, PeripheralsInterface):
        raise TypeError(
            'router: peripherals_interface should be an instance of '
            'PeripheralsInterface.')

    return_objects = []

    n_peripherals = peripherals_interface.n_peripherals

    peripheral_ens = []

    peripheral_rw_completes = []
    peripheral_r_datas = []

    for peripheral in peripherals_interface.peripherals:

        # Extract the peripheral interface from the peripherals interface
        peripheral_interface = (
            peripherals_interface.peripheral_interface_packager(peripheral))

        # Extract the rw_complete and r_data from the peripheral interface
        peripheral_rw_completes.append(peripheral_interface.rw_complete)
        peripheral_r_datas.append(peripheral_interface.r_data)

        peripheral_ens.append(Signal(False))

        # Create an address decoder to determine when the rw_addr is within
        # the address space of this peripheral.
        return_objects.append(
            address_decoder(
                core_memory_interface, peripheral_interface,
                peripheral_ens[-1]))

        # Create a peripheral_interface_controller for each peripheral
        return_objects.append(
            peripheral_interface_controller(
                core_memory_interface, peripheral_interface,
                peripheral_ens[-1]))

    @always_comb
    def mux():

        # By default, set the rw_complete signal low and the r_data to 0
        core_memory_interface.rw_complete.next = False
        core_memory_interface.r_data.next = 0

        for n in range(n_peripherals):
            if peripheral_ens[n]:
                # Connect the rw_complete and r_data signals from the
                # peripheral currently requesting access.
                core_memory_interface.rw_complete.next = (
                    peripheral_rw_completes[n])
                core_memory_interface.r_data.next = peripheral_r_datas[n]

    return_objects.append(mux)

    return return_objects
