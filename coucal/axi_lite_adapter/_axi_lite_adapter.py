from myhdl import block, enum, Signal, always

from kea.axi import AxiLiteInterface
from kea.utils import constant_assigner

from coucal.router import PeripheralInterface

@block
def axi_lite_adapter(clock, mem_interface, axil_interface):
    ''' An Axi lite adapter. This block is designed to work with the
    router. The router will drive the `mem_interface.rw_enable` signal
    correctly as determined by the routing bits of the `mem_interface.rw_addr`
    signal. If this block is not used with the router then it may not function
    correctly as it only checks enough bits of the `mem_interface.rw_addr` to
    get the offset within its own address space.
    '''

    if not isinstance(mem_interface, PeripheralInterface):
        raise TypeError(
            'axi_lite_adapter: The mem_interface should be an instance of '
            'PeripheralInterface.')

    if not isinstance(axil_interface, AxiLiteInterface):
        raise TypeError(
            'axi_lite_adapter: The axil_interface should be an instance of '
            'AxiLiteInterface.')

    if len(axil_interface.AWADDR) < len(mem_interface.rw_addr):
        raise ValueError(
            'axi_lite_adapter: The axil_interface address should be at least '
            'as wide as the mem_interface address.')

    if len(axil_interface.WDATA) != mem_interface.data_bitwidth:
        raise ValueError(
            'axi_lite_adapter: The mem_interface and the axil_interface '
            'should have the same data bitwidths.')

    if not hasattr(axil_interface, 'WSTRB'):
        raise ValueError(
            'axi_lite_adapter: The axil_interface should contain a write '
            'strobe.')

    # Get the data bitwidth and byte width
    data_bitwidth = mem_interface.data_bitwidth
    data_bytewidth = mem_interface.data_bytewidth

    # Sanity checks: These should be checked in the interfaces but we include
    # sanity checks here to make sure
    #
    # Check the read and write datas are the same width
    assert(len(mem_interface.r_data) == len(mem_interface.w_data))
    # Check data width is a multiple of 8
    assert(data_bitwidth % 8 == 0)
    # Check w_strobe is the correct width (equal to the data byte width)
    assert(len(mem_interface.w_strobe) == data_bytewidth)
    # Check the axil_interface read and write datas are the same width
    assert(len(axil_interface.WDATA) == len(axil_interface.RDATA))
    # Check the axil_interface read and write addresses are the same width
    assert(len(axil_interface.AWADDR) == len(axil_interface.ARADDR))
    # Check w_strobe is the correct width (equal to the data byte width)
    assert(len(axil_interface.WSTRB) == data_bytewidth)

    return_objects = []

    if hasattr(axil_interface, 'AWPROT'):
        return_objects.append(constant_assigner(0, axil_interface.AWPROT))

    if hasattr(axil_interface, 'ARPROT'):
        return_objects.append(constant_assigner(0, axil_interface.ARPROT))

    # RRESP and BRESP are unused. Set read on these signals to suppress the
    # conversion warnings that the signals are driven but not read.
    axil_interface.RRESP.read = True
    axil_interface.BRESP.read = True

    t_state = enum('AWAIT_TRANSACTION', 'READ', 'WRITE', 'COMPLETE')
    state = Signal(t_state.AWAIT_TRANSACTION)

    @always(clock.posedge)
    def control():

        # Always forward the rw_addr.
        axil_interface.AWADDR.next = mem_interface.rw_addr
        axil_interface.ARADDR.next = mem_interface.rw_addr

        # Always forward the write data and strobe
        axil_interface.WDATA.next = mem_interface.w_data
        axil_interface.WSTRB.next = mem_interface.w_strobe

        # Always forward the read data
        mem_interface.r_data.next = axil_interface.RDATA

        if state == t_state.AWAIT_TRANSACTION:

            if mem_interface.rw_enable:
                if mem_interface.w_strobe == 0:
                    # A read transaction
                    axil_interface.ARVALID.next = True
                    axil_interface.RREADY.next = True

                    state.next = t_state.READ

                else:
                    # A write transaction
                    axil_interface.AWVALID.next = True
                    axil_interface.WVALID.next = True
                    axil_interface.BREADY.next = True

                    state.next = t_state.WRITE

        elif state == t_state.READ:
            if axil_interface.ARREADY:
                # Read address has been received
                axil_interface.ARVALID.next = False

            if axil_interface.RVALID:
                # Read transaction has completed. Note: The other end must
                # wait for both ARVALID and ARREADY to go high before
                # asserting RVALID. Therefore we know the transaction has
                # completed when RVALID goes high.
                axil_interface.RREADY.next = False
                mem_interface.rw_complete.next = True

                state.next = t_state.COMPLETE

        elif state == t_state.WRITE:
            if axil_interface.AWREADY:
                # Write address has been received
                axil_interface.AWVALID.next = False

            if axil_interface.WREADY:
                # Write data has been received
                axil_interface.WVALID.next = False

            if axil_interface.BVALID:
                # Write transaction has completed. Note: The other end must
                # wait for AWVALID, AWREADY, WVALID and WREADY to go high
                # before asserting BVALID. Therefore we know the transaction
                # has completed when BVALID goes high.
                axil_interface.BREADY.next = False
                mem_interface.rw_complete.next = True

                state.next = t_state.COMPLETE

        elif state == t_state.COMPLETE:
            # Transaction complete
            mem_interface.rw_complete.next = False
            state.next = t_state.AWAIT_TRANSACTION

    return_objects.append(control)

    return return_objects
