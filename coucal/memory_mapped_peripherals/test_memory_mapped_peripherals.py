import random

from math import log

from myhdl import (
    Signal, block, always, enum, intbv, always_comb, StopSimulation)

from kea.test_utils import KeaTestCase as TestCase
from kea.test_utils import KeaVivadoVHDLTestCase as VivadoVHDLTestCase
from kea.test_utils import KeaVivadoVerilogTestCase as VivadoVerilogTestCase

from coucal.test_utils import random_string_generator
from coucal.core import CoreMemoryInterface, CORE_MEMORY_INTERFACE_TYPES
from coucal.router import random_address_map_generator
from coucal.memory_mapped_io import IOInterface
from coucal.axi_lite_adapter import (
    generate_axil_interface_and_types, axi_lite_slave_responder)

from ._memory_mapped_peripherals import (
    memory_mapped_peripherals, ROM, RAM, MEMORY_MAPPED_IO, AXI_LITE_ADAPTER,
    AVAILABLE_PERIPHERALS)

def generate_address_map(peripherals, n_bytes_upper_bound, zero_one_offset):
    ''' Generates a memory map which contains the requested peripherals with
    random offsets and n_bytes.
    '''

    for peripheral in peripherals:
        assert(peripheral in AVAILABLE_PERIPHERALS)

    # Generate a random address map with the correct number of peripherals
    n_peripherals = len(peripherals)
    address_map = (
        random_address_map_generator(
            n_peripherals, n_bytes_upper_bound, zero_one_offset))

    address_map_old_keys = list(address_map.keys())

    # Check the random address map has the correct number of peripherals
    assert(len(address_map_old_keys) == n_peripherals)

    for new_key, old_key in zip(peripherals, address_map_old_keys):

        # Replace the random peripheral names in the address map with the
        # requested peripheral names
        address_map[new_key] = address_map.pop(old_key)

    return address_map

def test_args_setup(peripherals, zero_one_offset=False):
    ''' Generate the arguments and argument types for the DUT.
    '''

    clock = Signal(False)
    core_memory_interface = CoreMemoryInterface()

    # Set an upper bound on the n_bytes so the tests don't take too long to
    # run
    n_bytes_upper_bound = 2**8 + 1

    # Generate an address map with all available peripherals included
    address_map = (
        generate_address_map(
            peripherals, n_bytes_upper_bound, zero_one_offset))

    args = {
        'clock': clock,
        'core_memory_interface': core_memory_interface,
        'address_map': address_map,
    }

    arg_types = {
        'clock': 'clock',
        'core_memory_interface': CORE_MEMORY_INTERFACE_TYPES,
        'address_map': 'non-signal',
    }

    # Sanity check the data widths
    assert(core_memory_interface.w_data == core_memory_interface.r_data)
    data_bitwidth = len(core_memory_interface.w_data)
    addr_bitwidth = len(core_memory_interface.rw_addr)

    if ROM in peripherals:
        # Generate the ROM data
        args['rom_data'] = [
            random.randrange(2**8) for n in
            range(address_map[ROM]['n_bytes'])]

        arg_types['rom_data'] = 'non-signal'

    if MEMORY_MAPPED_IO in peripherals:

        assert(data_bitwidth % 8 == 0)
        data_bytewidth = data_bitwidth//8

        mmio_n_bytes = address_map[MEMORY_MAPPED_IO]['n_bytes']
        assert(mmio_n_bytes % data_bytewidth == 0)

        # Define the maximum number of bytes assigned to the MMIO. Sanity
        # check that this value is a power of 2 (all peripheral n_bytes need
        # to be a power of 2).
        mmio_max_n_bytes = 32
        assert(mmio_max_n_bytes & (mmio_max_n_bytes-1) == 0)

        if mmio_n_bytes > mmio_max_n_bytes:
            # Limit the n_bytes assigned to the MMIO so that the tests have a
            # greater chance of modifying the registers.
            address_map[MEMORY_MAPPED_IO]['n_bytes'] = mmio_max_n_bytes

        # Calculate the upper bound on the number of registers possible given
        # the n_bytes assigned to the MMIO
        mmio_n_regs_upper_bound = (
            address_map[MEMORY_MAPPED_IO]['n_bytes']//data_bytewidth) + 1

        # Choose a random number of IO registers
        n_registers = random.randrange(1, mmio_n_regs_upper_bound)

        io_signal_definitions = []
        io_interface_types = {}

        for reg_n in range(n_registers):
            # Set up the signal definitions for each register. We only test
            # the rw registers here. The memory_mapped_io block is thoroughly
            # tested elsewhere. In this set of tests we are only testing the
            # interactions between blocks so rw registers should be sufficient
            # for that.
            register_signal_definitions = {
                'type': 'rw',
                'signals': []}

            for sig_n in range(data_bitwidth):
                # Generate a unique signal name for each signal
                sig_name = 'rw_reg_' + str(reg_n) + '_sig_' + str(sig_n)

                register_signal_definitions['signals'].append(sig_name)

                # Sanity check to make sure the registers are RW, then set up
                # the io interface types
                assert(register_signal_definitions['type'] == 'rw')
                io_interface_types[sig_name] = 'output'

            io_signal_definitions.append(register_signal_definitions)

        args['io_interface'] = IOInterface(io_signal_definitions)
        arg_types['io_interface'] = io_interface_types

    if AXI_LITE_ADAPTER in peripherals:

        args['axil_interface'], arg_types['axil_interface'] = (
            generate_axil_interface_and_types(
                data_bitwidth, addr_bitwidth, use_AWPROT=False,
                use_ARPROT=False, use_WSTRB=True))

    return args, arg_types

class TestMemoryMappedPeripheralsInterface(TestCase):
    ''' The DUT should reject incompatible interfaces and arguments.
    '''

    def setUp(self):

        self.args, _arg_types = test_args_setup(AVAILABLE_PERIPHERALS)

    def test_invalid_peripheral(self):
        ''' The `memory_mapped_peripherals` block should raise an error if the
        `address_map` contains a peripheral name which is not available.
        '''

        data_bitwidth = len(self.args['core_memory_interface'].w_data)
        assert(data_bitwidth % 8 == 0)
        data_bytewidth = data_bitwidth//8

        self.args['address_map'] = {
            random_string_generator(): {
                'n_bytes': data_bytewidth,
                'offset': 0,
            }}

        self.assertRaisesRegex(
            ValueError,
            ('memory_mapped_peripherals: Invalid peripheral. All '
             'peripherals should be one of ' +
             ', '.join(AVAILABLE_PERIPHERALS) + '.'),
            memory_mapped_peripherals,
            **self.args
        )

    def test_missing_rom_data(self):
        ''' The `memory_mapped_peripherals` block should raise an error if the
        `address_map` contains an entry for a ROM but the `rom_data` argument
        is not provided.
        '''

        del(self.args['rom_data'])

        self.assertRaisesRegex(
            ValueError,
            ('memory_mapped_peripherals: A ROM has been included in the '
             'address_map so rom_data should be provided.'),
            memory_mapped_peripherals,
            **self.args
        )

    def test_missing_io_interface(self):
        ''' The `memory_mapped_peripherals` block should raise an error if the
        `address_map` contains an entry for a memory mapped IO block but the
        `io_interface` argument is not provided.
        '''

        del(self.args['io_interface'])

        self.assertRaisesRegex(
            ValueError,
            ('memory_mapped_peripherals: A memory_mapped_io block has '
             'been included in the address_map so an io_interface should '
             'be provided.'),
            memory_mapped_peripherals,
            **self.args
        )

    def test_missing_axil_interface(self):
        ''' The `memory_mapped_peripherals` block should raise an error if the
        `address_map` contains an entry for an AXI lite adapter block but the
        `axil_interface` argument is not provided.
        '''

        del(self.args['axil_interface'])

        self.assertRaisesRegex(
            ValueError,
            ('memory_mapped_peripherals: An axi_lite_adapter block has '
             'been included in the address_map so an axil_interface '
             'should be provided.'),
            memory_mapped_peripherals,
            **self.args
        )

def new_mem_val(old_val, w_data, w_strobe):
    ''' This function returns the value to write to memory given the w_data
    and w_strobe signals.
    '''

    data_bitwidth = len(w_data)

    assert(data_bitwidth % 8 == 0)
    assert(data_bitwidth//8 == len(w_strobe))

    val = 0

    for n in range(len(w_strobe)):
        if w_strobe[n]:
            val |= w_data[8*(n+1):8*n] << n*8

        else:
            val |= old_val[8*(n+1):8*n] << n*8

    return val

class TestMemoryMappedPeripherals(TestCase):

    def setUp(self):

        self.args, self.arg_types = test_args_setup(AVAILABLE_PERIPHERALS)

        self.test_count = 0
        self.tests_run = False

    @block
    def end_tests(self, n_tests, **kwargs):

        clock = kwargs['clock']

        return_objects = []

        @always(clock.posedge)
        def control():

            if self.test_count >= n_tests:
                self.tests_run = True
                raise StopSimulation

        return_objects.append(control)

        return return_objects

    def random_stim_values(
        self, address_map, w_strobe_upper_bound, w_data_upper_bound,
        rw_addr_upper_bound):
        ''' This function generates stim values for the mem_interface inputs.
        '''

        w_data_val = random.randrange(w_data_upper_bound)

        if bool(random.randrange(2)):
            # Half the time set w_strobe to 0 so the DUT performs a read
            w_strobe_val = 0

        else:
            # Half the time set w_strobe to a non 0 value so the DUT
            # performs a write
            w_strobe_val = random.randrange(1, w_strobe_upper_bound)

        # Pick a random peripheral and return a rw_addr which is within the
        # address space of that peripheral
        peripheral = random.choice(list(address_map.keys()))

        peripheral_offset = address_map[peripheral]['offset']
        peripheral_addr_upper_bound = (
            peripheral_offset + address_map[peripheral]['n_bytes'])
        peripheral_max_addr = peripheral_addr_upper_bound-1

        p = random.random()

        if p < 0.2:
            rw_addr_val = peripheral_offset

        elif p < 0.4:
            rw_addr_val = peripheral_max_addr

        else:
            rw_addr_val = random.randrange(
                peripheral_offset, peripheral_addr_upper_bound)

        return w_strobe_val, w_data_val, rw_addr_val

    @block
    def core_mem_interface_stim(
        self, clock, core_mem_interface, address_map, hold_rw_enable_high):
        ''' This block randomly drives the core_mem_interface.
        '''

        assert(isinstance(core_mem_interface, CoreMemoryInterface))

        return_objects = []

        rw_addr_upper_bound = 2**len(core_mem_interface.rw_addr)

        w_strobe_upper_bound = 2**len(core_mem_interface.w_strobe)
        w_data_upper_bound = 2**len(core_mem_interface.w_data)

        t_state = enum('STIM', 'AWAIT_COMPLETE')
        state = Signal(t_state.STIM)

        @always(clock.posedge)
        def stim():

            if state == t_state.STIM:
                # Generate random stim values
                w_strobe_val, w_data_val, rw_addr_val = (
                    self.random_stim_values(
                        address_map, w_strobe_upper_bound, w_data_upper_bound,
                        rw_addr_upper_bound))

                # Whilst rw_enable is low keep updating the other stim signals
                # with random values.
                core_mem_interface.rw_addr.next = rw_addr_val
                core_mem_interface.w_strobe.next = w_strobe_val
                core_mem_interface.w_data.next = w_data_val

                if random.random() < 0.2:
                    # Randomly set rw_enable
                    core_mem_interface.rw_enable.next = True
                    state.next = t_state.AWAIT_COMPLETE

            elif state == t_state.AWAIT_COMPLETE:
                if core_mem_interface.rw_complete:
                    # Generate random stim values
                    w_strobe_val, w_data_val, rw_addr_val = (
                        self.random_stim_values(
                            address_map, w_strobe_upper_bound,
                            w_data_upper_bound, rw_addr_upper_bound))

                    core_mem_interface.rw_addr.next = rw_addr_val
                    core_mem_interface.w_strobe.next = w_strobe_val
                    core_mem_interface.w_data.next = w_data_val

                    if hold_rw_enable_high or random.random() < 0.1:
                        # Keep rw_enable high and set up another read
                        core_mem_interface.rw_enable.next = True

                    else:
                        core_mem_interface.rw_enable.next = False
                        state.next = t_state.STIM

        return_objects.append(stim)

        return return_objects

    @block
    def memory_mapped_peripherals_check(
        self, hold_rw_enable_high=False, **kwargs):

        clock = kwargs['clock']
        core_memory_interface = kwargs['core_memory_interface']
        address_map = kwargs['address_map']

        rw_enable = core_memory_interface.rw_enable
        rw_complete = core_memory_interface.rw_complete
        rw_addr = core_memory_interface.rw_addr
        w_strobe = core_memory_interface.w_strobe
        w_data = core_memory_interface.w_data
        r_data = core_memory_interface.r_data

        data_bitwidth = len(w_data)
        assert(data_bitwidth % 8 == 0)
        data_bytewidth = data_bitwidth//8

        # The rw_addr is a byte address. Calculate the number of bits in the
        # rw_addr which are used to specify a byte.
        byte_addr_bitwidth = log(data_bytewidth)/log(2)

        addr_bitwidth = len(rw_addr)

        peripherals = list(address_map.keys())

        return_objects = []

        # Create a stim block
        return_objects.append(
            self.core_mem_interface_stim(
                clock, core_memory_interface, address_map,
                hold_rw_enable_high))

        if ROM in peripherals:
            rom_data_bytes = kwargs['rom_data']
            rom_data_words = []

            for m in range(0, len(rom_data_bytes), data_bytewidth):
                rom_word = 0

                for n in range(data_bytewidth):
                    # Extract the relevant bytes from the ROM data and
                    # assemble into a word
                    rom_byte = rom_data_bytes[m+n]
                    rom_word |= rom_byte << n*8

                # Keep a record of the rom data in word form
                rom_data_words.append(rom_word)

            # Extract the ROM address range
            rom_n_bytes = address_map[ROM]['n_bytes']
            rom_addr_lower_bound = address_map[ROM]['offset']
            rom_addr_upper_bound = rom_addr_lower_bound + rom_n_bytes

            # Calculate how many bits of the rw_addr are required to get the
            # offset within the ROM address space.
            rom_offset_bitwidth = log(rom_n_bytes)/log(2)

            if rom_offset_bitwidth > byte_addr_bitwidth:
                rom_offset = rw_addr(rom_offset_bitwidth, byte_addr_bitwidth)

            else:
                rom_offset = 0

            @always(clock.posedge)
            def check_rom():

                if (rw_enable and
                    rw_complete and
                    rw_addr >= rom_addr_lower_bound and
                    rw_addr < rom_addr_upper_bound):

                    assert(r_data == rom_data_words[rom_offset])

                    self.test_count += 1

            return_objects.append(check_rom)

        if RAM in peripherals:
            # Extract the RAM address range
            ram_n_bytes = address_map[RAM]['n_bytes']
            ram_addr_lower_bound = address_map[RAM]['offset']
            ram_addr_upper_bound = ram_addr_lower_bound + ram_n_bytes

            # Create a list to model the expected RAM contents
            assert(ram_n_bytes % data_bytewidth == 0)
            ram_n_words = ram_n_bytes//data_bytewidth
            expected_ram = [Signal(intbv(0)) for n in range(ram_n_words)]

            # Calculate how many bits of the rw_addr are required to get the
            # offset within the RAM address space.
            ram_offset_bitwidth = log(ram_n_bytes)/log(2)

            if ram_offset_bitwidth > byte_addr_bitwidth:
                ram_offset = rw_addr(ram_offset_bitwidth, byte_addr_bitwidth)

            else:
                ram_offset = 0

            @always(clock.posedge)
            def check_ram():

                if (rw_enable and
                    rw_complete and
                    rw_addr >= ram_addr_lower_bound and
                    rw_addr < ram_addr_upper_bound):

                    if w_strobe == 0:
                        # Read in progress so check the read data
                        assert(r_data == expected_ram[ram_offset])

                    else:
                        # Write in progress so update the model RAM
                        expected_ram[ram_offset].next = (
                            new_mem_val(
                                expected_ram[ram_offset], w_data, w_strobe))

                    self.test_count += 1

            return_objects.append(check_ram)

        if MEMORY_MAPPED_IO in peripherals:
            io_interface = kwargs['io_interface']

            # Extract the memory mapped IO address range
            mmio_n_bytes = address_map[MEMORY_MAPPED_IO]['n_bytes']
            mmio_addr_lower_bound = address_map[MEMORY_MAPPED_IO]['offset']
            mmio_addr_upper_bound = mmio_addr_lower_bound + mmio_n_bytes

            # Create a list to model the expected MMIO registers
            mmio_n_regs = io_interface.n_registers
            expected_mmio_regs = [
                Signal(intbv(0)) for n in range(mmio_n_regs)]

            for n in range(mmio_n_regs):
                # Check that all registers are rw
                assert(io_interface.register_type(n) == 'rw')

            # Calculate how many bits of the rw_addr are required to get the
            # offset within the memory mapped IO address space.
            mmio_offset_bitwidth = log(mmio_n_bytes)/log(2)

            if mmio_offset_bitwidth > byte_addr_bitwidth:
                mmio_offset = rw_addr(mmio_offset_bitwidth, byte_addr_bitwidth)

            else:
                mmio_offset = 0

            @always(clock.posedge)
            def check_mmio():

                if (rw_enable and
                    rw_complete and
                    rw_addr >= mmio_addr_lower_bound and
                    rw_addr < mmio_addr_upper_bound):

                    if w_strobe == 0:
                        # Read in progress so check the read data
                        if mmio_offset < mmio_n_regs:
                            assert(r_data == expected_mmio_regs[mmio_offset])

                        else:
                            assert(r_data == 0)

                    else:
                        # Write in progress so update the expected MMIO
                        # registers
                        if mmio_offset < mmio_n_regs:
                            expected_mmio_regs[mmio_offset].next = (
                                new_mem_val(
                                    expected_mmio_regs[mmio_offset], w_data,
                                    w_strobe))

                    self.test_count += 1

            return_objects.append(check_mmio)

        if AXI_LITE_ADAPTER in peripherals:
            axil_interface = kwargs['axil_interface']

            # Extract the AXI lite adapter address range
            axil_n_bytes = address_map[AXI_LITE_ADAPTER]['n_bytes']
            axil_addr_lower_bound = address_map[AXI_LITE_ADAPTER]['offset']
            axil_addr_upper_bound = axil_addr_lower_bound + axil_n_bytes

            # Create a block to stimulate the axil_interface
            return_objects.append(
                axi_lite_slave_responder(clock, axil_interface))

            axil_write_addr = Signal(intbv(0)[addr_bitwidth:])
            axil_write_strobe = Signal(intbv(0)[len(w_strobe):])
            axil_read_addr = Signal(intbv(0)[addr_bitwidth:])
            axil_write_data = Signal(intbv(0)[data_bitwidth:])
            axil_read_data = Signal(intbv(0)[data_bitwidth:])

            @always(clock.posedge)
            def check_axil():

                if axil_interface.AWVALID and axil_interface.AWREADY:
                    # Keep a record of the write address used
                    axil_write_addr.next = axil_interface.AWADDR

                if axil_interface.WVALID and axil_interface.WREADY:
                    # Keep a record of the written data and strobe
                    axil_write_data.next = axil_interface.WDATA
                    axil_write_strobe.next = axil_interface.WSTRB

                if axil_interface.ARVALID and axil_interface.ARREADY:
                    # Keep a record of the read address used
                    axil_read_addr.next = axil_interface.ARADDR

                if axil_interface.RVALID and axil_interface.RREADY:
                    # Keep a record of the read data
                    axil_read_data.next = axil_interface.RDATA

                if (rw_enable and
                    rw_complete and
                    rw_addr >= axil_addr_lower_bound and
                    rw_addr < axil_addr_upper_bound):

                    if w_strobe == 0:
                        # Read in progress so check the read data
                        assert(r_data == axil_read_data)

                    else:
                        # Check that the DUT forwarded the w_data and w_strobe
                        # correctly
                        assert(axil_write_data == w_data)
                        assert(axil_write_strobe == w_strobe)

                    self.test_count += 1

            return_objects.append(check_axil)

        return return_objects

    def test_all_peripherals(self):
        ''' It should be possible to access all peripherals included in the
        `address_map` via the `core_memory_interface`.

        Note: There can only be one of each type of peripheral.
        '''

        cycles = 20000
        n_tests = 200

        # Sanity check to make sure all peripherals are included in the
        # address_map
        assert(list(self.args['address_map'].keys()) == AVAILABLE_PERIPHERALS)

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(
                self.memory_mapped_peripherals_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, memory_mapped_peripherals, memory_mapped_peripherals,
            self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_random_peripherals(self):
        ''' It should be possible to create a `memory_mapped_peripherals`
        block with random peripherals enabled.
        '''

        cycles = 20000
        n_tests = 200

        n_peripherals = random.randrange(1, len(AVAILABLE_PERIPHERALS))
        peripherals = random.sample(AVAILABLE_PERIPHERALS, n_peripherals)

        self.args, self.arg_types = test_args_setup(peripherals)

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(
                self.memory_mapped_peripherals_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, memory_mapped_peripherals, memory_mapped_peripherals,
            self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_address_zero(self):
        ''' The `memory_mapped_peripherals` block should function correctly
        when the address space of one peripheral includes 0x0.
        '''

        cycles = 20000
        n_tests = 200

        peripherals = [random.choice(AVAILABLE_PERIPHERALS)]
        self.args, self.arg_types = (
            test_args_setup(peripherals, zero_one_offset=True))

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(
                self.memory_mapped_peripherals_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, memory_mapped_peripherals, memory_mapped_peripherals,
            self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_max_address(self):
        ''' The `memory_mapped_peripherals` block should function correctly
        when the address space of one peripheral includes the highest address
        in the address space.
        '''

        cycles = 20000
        n_tests = 200

        peripherals = [random.choice(AVAILABLE_PERIPHERALS)]
        self.args, self.arg_types = test_args_setup(peripherals)

        peripheral_n_bytes = (
            self.args['address_map'][peripherals[0]]['n_bytes'])
        address_space_upper_bound = (
            2**len(self.args['core_memory_interface'].rw_addr))

        # Modify the offset of the peripheral so it includes the maximum
        # address
        self.args['address_map'][peripherals[0]]['offset'] = (
            address_space_upper_bound - peripheral_n_bytes)

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(
                self.memory_mapped_peripherals_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, memory_mapped_peripherals, memory_mapped_peripherals,
            self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_back_to_back_reads_and_writes(self):
        ''' The `memory_mapped_peripherals` block should function correctly
        when back to back reads from and writes to different addresses are
        requested.
        '''
        cycles = 20000
        n_tests = 200

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(
                self.memory_mapped_peripherals_check(
                    **kwargs, hold_rw_enable_high=True))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, memory_mapped_peripherals, memory_mapped_peripherals,
            self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

class TestMemoryMappedPeripheralsVivadoVhdl(
    VivadoVHDLTestCase, TestMemoryMappedPeripherals):
    pass

class TestMemoryMappedPeripheralsVivadoVerilog(
    VivadoVerilogTestCase, TestMemoryMappedPeripherals):
    pass
