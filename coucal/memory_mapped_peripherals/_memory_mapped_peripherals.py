from myhdl import block, always

from coucal.router import PeripheralsInterface, router
from coucal.rom import rom
from coucal.ram import ram
from coucal.memory_mapped_io import memory_mapped_io
from coucal.axi_lite_adapter import axi_lite_adapter

ROM = 'rom'
RAM = 'ram'
MEMORY_MAPPED_IO = 'memory_mapped_io'
AXI_LITE_ADAPTER = 'axi_lite_adapter'

AVAILABLE_PERIPHERALS = [ROM, RAM, MEMORY_MAPPED_IO, AXI_LITE_ADAPTER]

@block
def memory_mapped_peripherals(
    clock, core_memory_interface, address_map, rom_data=None,
    io_interface=None, axil_interface=None):
    ''' This block instantiates the required memory mapped peripherals.
    '''

    for peripheral in address_map:
        if peripheral not in AVAILABLE_PERIPHERALS:
            raise ValueError(
                'memory_mapped_peripherals: Invalid peripheral. All '
                'peripherals should be one of ' +
                ', '.join(AVAILABLE_PERIPHERALS) + '.')

    return_objects = []

    # Create a list of the requested peripherals
    requested_peripherals = [peripheral for peripheral in address_map]

    # Create the peripherals interface
    peripherals_interface = PeripheralsInterface(address_map)

    # Create the router
    return_objects.append(
        router(core_memory_interface, peripherals_interface))

    if ROM in requested_peripherals:

        if rom_data is None:
            raise ValueError(
                'memory_mapped_peripherals: A ROM has been included in the '
                'address_map so rom_data should be provided.')

        # Extract the ROM interface
        rom_interface = (
            peripherals_interface.peripheral_interface_packager(ROM))

        # Create the ROM
        return_objects.append(rom(clock, rom_interface, rom_data))

    if RAM in requested_peripherals:

        # Extract the RAM interface
        ram_interface = (
            peripherals_interface.peripheral_interface_packager(RAM))

        # Create the RAM
        return_objects.append(ram(clock, ram_interface))

    if MEMORY_MAPPED_IO in requested_peripherals:

        if io_interface is None:
            raise ValueError(
                'memory_mapped_peripherals: A memory_mapped_io block has '
                'been included in the address_map so an io_interface should '
                'be provided.')

        # Extract the mem interface
        mem_interface = (
            peripherals_interface.peripheral_interface_packager(
                MEMORY_MAPPED_IO))

        # Create the memory mapped IO
        return_objects.append(
            memory_mapped_io(clock, mem_interface, io_interface))

    if AXI_LITE_ADAPTER in requested_peripherals:

        if axil_interface is None:
            raise ValueError(
                'memory_mapped_peripherals: An axi_lite_adapter block has '
                'been included in the address_map so an axil_interface '
                'should be provided.')

        # Extract the mem interface
        mem_interface = (
            peripherals_interface.peripheral_interface_packager(
                AXI_LITE_ADAPTER))

        # Create the axi lite adapter
        return_objects.append(
            axi_lite_adapter(clock, mem_interface, axil_interface))

    return return_objects
