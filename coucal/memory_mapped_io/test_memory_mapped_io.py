import random

from collections import deque
from math import log

from myhdl import Signal, block, always, enum, intbv, StopSimulation

from kea.test_utils import KeaTestCase as TestCase
from kea.test_utils import KeaVivadoVHDLTestCase as VivadoVHDLTestCase
from kea.test_utils import KeaVivadoVerilogTestCase as VivadoVerilogTestCase

from coucal.test_utils import random_string_generator
from coucal.router import (
    PeripheralsInterface, PeripheralInterface, random_address_map_generator)

from .interfaces import AVAILABLE_IO_TYPES, IOInterface
from ._memory_mapped_io import memory_mapped_io

def generate_io_interface(
    min_n_registers=1, n_registers_upper_bound=10, min_n_signals_per_reg=1,
    n_signals_per_reg_upper_bound=33):
    ''' Generates an IOinterface and the types for that IOInterface.
    '''

    assert(min_n_registers > 0)
    assert(min_n_signals_per_reg > 0)

    # Pick a random number of registers
    n_registers = random.randrange(min_n_registers, n_registers_upper_bound)

    # Generate a random number of signals for each register
    registers_n_signals = [
        random.randrange(min_n_signals_per_reg, n_signals_per_reg_upper_bound)
        for n in range(n_registers)]

    # Calculate the total number of signals on the interface
    n_signals_total = sum(registers_n_signals)

    signal_names = deque([])

    for n in range(n_signals_total):
        # Generate random names for the signals
        signal_name = random_string_generator(8)

        if signal_name in signal_names:
            # Check that the signal name is unique if not we try again
            regenerate_attempts_upper_bound = 100

            for n in range(regenerate_attempts_upper_bound):
                # Try generating a new signal name
                signal_name = random_string_generator(8)

                if signal_name not in signal_names:
                    # The new signal_name is unique so use it.
                    break

                else:
                    # We can have up to regenerate_attempts_upper_bound
                    assert(n < regenerate_attempts_upper_bound)

        # Add the unique signal name to the list
        signal_names.append(signal_name)

    io_signal_definitions = []
    io_interface_types = {}

    for n in range(n_registers):
        # Randomly select a type for the register
        register_type = random.choice(AVAILABLE_IO_TYPES)

        # Pick a random number of signals on the register
        n_register_signals = registers_n_signals[n]

        # Extract signal names for this register
        register_signals = [
            signal_names.popleft() for n in range(n_register_signals)]

        if register_type == 'rw' or register_type == 'wo':
            signal_type = 'output'

        elif register_type == 'ro':
            signal_type = 'custom'

        else:
            raise ValueError('generate_io_interface: Invalid register type.')

        for signal in register_signals:
            # Add the signal to the io_interface_types dict
            io_interface_types[signal] = signal_type

        io_signal_definitions.append({
            'type': register_type,
            'signals': register_signals})

    io_interface = IOInterface(io_signal_definitions)

    return io_interface, io_interface_types

def generate_mem_interface(offset=None, n_bytes=None):
    ''' Generate the mem_interface.
    '''

    n_peripherals = 1

    # Set an upper bound on the n_bytes so the tests don't take too long to
    # run
    n_bytes_upper_bound = 2**8 + 1

    zero_one_offset = bool(random.randrange(2))

    # Create a random address map
    address_map = (
        random_address_map_generator(
            n_peripherals, n_bytes_upper_bound, zero_one_offset))

    # The address should contain a single peripheral as we only need one for
    # peripheral interface for these tests.
    assert(len(address_map) == 1)

    if offset is not None:
        # Update the address map with the specified offset
        peripheral_name = list(address_map.keys())[0]
        address_map[peripheral_name]['offset'] = offset

    if n_bytes is not None:
        # Update the address map with the specified n_bytes
        peripheral_name = list(address_map.keys())[0]
        address_map[peripheral_name]['n_bytes'] = n_bytes

    # Create the peripherals interface
    peripherals_interface = PeripheralsInterface(address_map)

    peripheral_name = peripherals_interface.peripherals[0]
    mem_interface = (
        peripherals_interface.peripheral_interface_packager(peripheral_name))

    return mem_interface

def test_args_setup():
    ''' Generate the arguments and argument types for the DUT.
    '''

    clock = Signal(False)

    mem_interface = generate_mem_interface()

    # Get the data bitwidth and bytewidth
    data_bitwidth = mem_interface.data_bitwidth
    data_bytewidth = mem_interface.data_bytewidth

    n_registers_upper_bound = mem_interface.n_bytes//data_bytewidth + 1

    io_interface, io_interface_types = (
        generate_io_interface(
            n_registers_upper_bound=n_registers_upper_bound))

    args = {
        'clock': clock,
        'mem_interface': mem_interface,
        'io_interface': io_interface,
    }

    mem_interface_types = {
        'rw_enable': 'custom',
        'rw_complete': 'output',
        'rw_addr': 'custom',
        'w_strobe': 'custom',
        'w_data': 'custom',
        'r_data': 'output',
    }

    arg_types = {
        'clock': 'clock',
        'mem_interface': mem_interface_types,
        'io_interface': io_interface_types,
    }

    return args, arg_types

class TestMemoryMappedIOInterface(TestCase):
    ''' The DUT should reject incompatible interfaces and arguments.
    '''

    def setUp(self):

        self.args, _arg_types = test_args_setup()

    def test_invalid_mem_interface(self):
        ''' The `memory_mapped_io` should raise an error if the
        `mem_interface` is not an instance of `PeripheralInterface`.
        '''

        self.args['mem_interface'] = random.randrange(0, 100)

        self.assertRaisesRegex(
            TypeError,
            ('memory_mapped_io: The mem_interface should be an instance of '
             'PeripheralInterface.'),
            memory_mapped_io,
            **self.args
        )

    def test_invalid_io_interface(self):
        ''' The `memory_mapped_io` should raise an error if the
        `io_interface` is not an instance of `PeripheralInterface`.
        '''

        self.args['io_interface'] = random.randrange(0, 100)

        self.assertRaisesRegex(
            TypeError,
            ('memory_mapped_io: The io_interface should be an instance of '
             'IOInterface.'),
            memory_mapped_io,
            **self.args
        )

class TestMemoryMappedIO(TestCase):

    def setUp(self):

        self.args, self.arg_types = test_args_setup()

        self.test_count = 0
        self.tests_run = False

    @block
    def end_tests(self, n_tests, **kwargs):

        clock = kwargs['clock']

        return_objects = []

        @always(clock.posedge)
        def control():

            if self.test_count >= n_tests:
                self.tests_run = True
                raise StopSimulation

        return_objects.append(control)

        return return_objects

    def random_stim_values(
        self, w_strobe_upper_bound, w_data_upper_bound,
        io_offset, io_addr_upper_bound, rw_addr_upper_bound):
        ''' This function generates stim values for the mem_interface inputs.
        '''

        io_addr_space_max = io_addr_upper_bound - 1
        rw_addr_max_val = rw_addr_upper_bound - 1

        w_data_val = random.randrange(w_data_upper_bound)

        if bool(random.randrange(2)):
            # Half the time set w_strobe to 0 so the DUT performs a read
            w_strobe_val = 0

        else:
            # Half the time set w_strobe to a non 0 value so the DUT
            # performs a write
            w_strobe_val = random.randrange(1, w_strobe_upper_bound)

        random_val = random.random()

        if random_val < 0.05:
            # Set rw_addr to 0 five per cent of the time
            rw_addr_val = 0

        elif random_val < 0.1:
            # Set rw_addr to its max value five per cent of the time
            rw_addr_val = rw_addr_max_val

        elif random_val < 0.15:
            # Set rw_addr to the IO offset five per cent of the time
            rw_addr_val = io_offset

        elif random_val < 0.2:
            # Set rw_addr to the highest address in the IO address space
            # five per cent of the time
            rw_addr_val = io_addr_space_max

        elif random_val < 0.25 and (
            io_addr_upper_bound < rw_addr_upper_bound):
            # If the next address after the DUT address space is a valid
            # address then set rw_addr to it five per cent of the time.
            rw_addr_val = io_addr_upper_bound

        elif random_val < 0.5:
            # Randomly drive rw_addr twenty five per cent of the time
            rw_addr_val = random.randrange(rw_addr_upper_bound)

        else:
            # Select a random value in the DUT address space fifty per
            # cent of the time.
            rw_addr_val = (
                random.randrange(io_offset, io_addr_upper_bound))

        return w_strobe_val, w_data_val, rw_addr_val

    @block
    def mem_interface_stim(self, clock, mem_interface, hold_rw_enable_high):
        ''' This block randomly drives the mem_interface.
        '''

        assert(isinstance(mem_interface, PeripheralInterface))

        return_objects = []

        io_offset = mem_interface.offset
        io_n_bytes = mem_interface.n_bytes

        io_addr_upper_bound = io_offset + io_n_bytes
        rw_addr_upper_bound = 2**len(mem_interface.rw_addr)

        w_strobe_upper_bound = 2**len(mem_interface.w_strobe)
        w_data_upper_bound = 2**len(mem_interface.w_data)

        t_state = enum('STIM', 'AWAIT_COMPLETE')
        state = Signal(t_state.STIM)

        @always(clock.posedge)
        def stim():

            if state == t_state.STIM:
                # Generate random stim values
                w_strobe_val, w_data_val, rw_addr_val = (
                    self.random_stim_values(
                        w_strobe_upper_bound, w_data_upper_bound,
                        io_offset, io_addr_upper_bound, rw_addr_upper_bound))

                # Whilst rw_enable is low keep updating the other stim signals
                # with random values.
                mem_interface.rw_addr.next = rw_addr_val
                mem_interface.w_strobe.next = w_strobe_val
                mem_interface.w_data.next = w_data_val

                if random.random() < 0.2:
                    # Randomly set rw_enable
                    mem_interface.rw_enable.next = True
                    state.next = t_state.AWAIT_COMPLETE

            elif state == t_state.AWAIT_COMPLETE:
                if mem_interface.rw_complete:
                    # Generate random stim values
                    w_strobe_val, w_data_val, rw_addr_val = (
                        self.random_stim_values(
                            w_strobe_upper_bound, w_data_upper_bound,
                            io_offset, io_addr_upper_bound,
                            rw_addr_upper_bound))

                    mem_interface.rw_addr.next = rw_addr_val
                    mem_interface.w_strobe.next = w_strobe_val
                    mem_interface.w_data.next = w_data_val

                    if hold_rw_enable_high or random.random() < 0.1:
                        # Keep rw_enable high and set up another read
                        mem_interface.rw_enable.next = True

                    else:
                        mem_interface.rw_enable.next = False
                        state.next = t_state.STIM

        return_objects.append(stim)

        return return_objects

    @block
    def io_interface_stim(self, clock, io_interface):
        ''' Drives the input IO with random values.
        '''

        return_objects = []

        input_io = []

        for n in range(io_interface.n_registers):
            if io_interface.register_type(n) == 'ro':
                # Gather all of the input IO into a single list
                input_io.extend(io_interface.register_signals(n))

        n_input_io = len(input_io)

        @always(clock.posedge)
        def stim():

            for n in range(n_input_io):
                input_io[n].next = bool(random.randrange(2))

        return_objects.append(stim)

        return return_objects

    @block
    def rw_register(
        self, clock, register_byte, rw_enable, byte_w_strobe, w_data,
        mem_addr, io_signals, register_address):
        ''' Models a 1 byte rw register.
        '''

        assert(len(register_byte) == 8)
        assert(len(w_data) == 8)
        assert(len(io_signals) <= 8)

        if isinstance(mem_addr, int):
            # If the mem_addr is an int then it should be set to zero. The
            # only time this is acceptable is if the register address is 0
            assert(register_address == 0)
            assert(mem_addr == 0)

        else:
            # Otherwise the register address should be accessible by mem_addr
            assert(register_address < 2**len(mem_addr))

        return_objects = []

        n_io_signals = len(io_signals)

        @always(clock.posedge)
        def control():

            if rw_enable and byte_w_strobe and mem_addr==register_address:
                for n in range(n_io_signals):
                    # Update the io signals and the register with w_data
                    io_signals[n].next = w_data[n]
                    register_byte.next[n] = w_data[n]

        return_objects.append(control)

        return return_objects

    @block
    def wo_register(
        self, clock, register_byte, rw_enable, byte_w_strobe, w_data,
        mem_addr, io_signals, register_address):
        ''' Models a 1 byte wo register.
        '''

        assert(len(register_byte) == 8)
        assert(len(w_data) == 8)
        assert(len(io_signals) <= 8)

        if isinstance(mem_addr, int):
            # If the mem_addr is an int then it should be set to zero. The
            # only time this is acceptable is if the register address is 0
            assert(register_address == 0)
            assert(mem_addr == 0)

        else:
            # Otherwise the register address should be accessible by mem_addr
            assert(register_address < 2**len(mem_addr))

        return_objects = []

        n_io_signals = len(io_signals)

        t_state = enum('AWAIT_WRITE', 'PROPAGATION')
        state = Signal(t_state.AWAIT_WRITE)

        @always(clock.posedge)
        def control():

            # Register should always read as 0
            register_byte.next = 0

            if state == t_state.AWAIT_WRITE:
                if rw_enable and byte_w_strobe and mem_addr==register_address:
                    for n in range(n_io_signals):
                        # Update the io signals with w_data
                        io_signals[n].next = w_data[n]

                    state.next = t_state.PROPAGATION

            elif state == t_state.PROPAGATION:
                # IO signals should only ever pulse
                for n in range(n_io_signals):
                    io_signals[n].next = 0

                state.next = t_state.AWAIT_WRITE

        return_objects.append(control)

        return return_objects

    @block
    def ro_register(self, clock, register_byte, io_signals):
        ''' Models a 1 byte ro register.
        '''

        assert(len(register_byte) == 8)
        assert(len(io_signals) <= 8)

        return_objects = []

        n_io_signals = len(io_signals)

        @always(clock.posedge)
        def control():

            for n in range(n_io_signals):
                # Update the register byte with the io signals
                register_byte.next[n] = io_signals[n]

        return_objects.append(control)

        return return_objects

    @block
    def memory_mapped_io_check(self, hold_rw_enable_high=False, **kwargs):

        clock = kwargs['clock']
        mem_interface = kwargs['mem_interface']
        io_interface = kwargs['io_interface']

        assert(isinstance(mem_interface, PeripheralInterface))
        assert(isinstance(io_interface, IOInterface))

        return_objects = []

        return_objects.append(
            self.mem_interface_stim(
                clock, mem_interface, hold_rw_enable_high))

        return_objects.append(self.io_interface_stim(clock, io_interface))

        assert(len(mem_interface.r_data) == len(mem_interface.w_data))

        data_bitwidth = mem_interface.data_bitwidth
        data_bytewidth = mem_interface.data_bytewidth

        n_registers = io_interface.n_registers

        if mem_interface.n_bytes//data_bytewidth == 1:
            # We have a single memory location
            mem_addr = 0

        else:
            # Calculate the lower index to convert from byte address to word
            # address
            mem_addr_lower_index = int(log(data_bytewidth)/log(2))

            # Extract the memory address from the rw_addr
            mem_addr = (
                mem_interface.rw_addr(
                    mem_interface.n_memory_addressing_bits,
                    mem_addr_lower_index))

        # Slice the w_data signal into bytes
        w_data_bytes = [
            mem_interface.w_data(8*(n+1), 8*n) for n in range(data_bytewidth)]

        memory = [Signal(intbv(0)[8:]) for n in range(mem_interface.n_bytes)]

        expected_rw_complete = Signal(False)
        expected_r_data = Signal(intbv(0)[data_bitwidth:])

        output_io = []
        expected_output_io = []

        for n in range(io_interface.n_registers):

            register_type = io_interface.register_type(n)

            # Extract the IO signals for this register from the
            # io_interface
            register_io_signals = io_interface.register_signals(n)
            register_io_chunks = [
                register_io_signals[n:n+8]
                for n in range(0, len(register_io_signals), 8)]

            if register_type == 'rw' or register_type == 'wo':

                n_register_io_signals = len(register_io_signals)

                # Create expected io for our model registers
                register_expected_output_io = [
                    Signal(False) for n in range(n_register_io_signals)]
                register_expected_output_chunks = [
                    register_expected_output_io[n:n+8]
                    for n in range(0, len(register_expected_output_io), 8)]

                # The registers are rw or wo so the IO associated with this
                # register are all outputs so we need to check them.
                output_io.extend(register_io_signals)
                expected_output_io.extend(register_expected_output_io)

                for m, register_expected_output_chunk in enumerate(
                    register_expected_output_chunks):

                    # Extract the register byte from memory
                    register_byte = memory[n*data_bytewidth+m]

                    if register_type == 'rw':
                        return_objects.append(
                            self.rw_register(
                                clock, register_byte, mem_interface.rw_enable,
                                mem_interface.w_strobe(m), w_data_bytes[m],
                                mem_addr, register_expected_output_chunk, n))

                    elif register_type == 'wo':
                        return_objects.append(
                            self.wo_register(
                                clock, register_byte, mem_interface.rw_enable,
                                mem_interface.w_strobe(m), w_data_bytes[m],
                                mem_addr, register_expected_output_chunk, n))

            elif register_type == 'ro':
                for m, register_io_chunk in enumerate(register_io_chunks):

                    # Extract the register byte from memory
                    register_byte = memory[n*data_bytewidth+m]

                    return_objects.append(
                        self.ro_register(
                            clock, register_byte, register_io_chunk))

            else:
                raise ValueError('Invalid register type.')

        assert(len(output_io) == len(expected_output_io))
        n_output_io = len(output_io)

        @always(clock.posedge)
        def check():

            assert(mem_interface.rw_complete == expected_rw_complete)
            assert(mem_interface.r_data == expected_r_data)

            for n in range(n_output_io):
                # Check the output IO is correct
                assert(output_io[n] == expected_output_io[n])

            # The DUT should set rw_complete in response to rw_enable. If
            # rw_complete is high then the DUT should wait one cycle for the
            # response to propagate.
            expected_rw_complete.next = (
                mem_interface.rw_enable and not mem_interface.rw_complete)

            if (mem_interface.rw_enable and
                mem_interface.w_strobe == 0 and
                not mem_interface.rw_complete):
                # A read in progress
                mem_val = 0

                for n in range(data_bytewidth):
                    # Shift each memory location into the correct offset
                    # in the expected data
                    mem_val |= memory[(mem_addr*data_bytewidth) + n] << n*8

                expected_r_data.next = mem_val

            if expected_rw_complete:
                self.test_count += 1

        return_objects.append(check)

        return return_objects

    def test_io(self):
        ''' The `memory_mapped_io` block should contain an address space of
        length `mem_interface.n_bytes`. Within that address space, n addresses
        are allocated to the registers specified by the `io_interface`. All
        other addresses are unused.

        The `io_interface` contains the IO signals. These signals should be
        connected to specific registers. Each register corresponds to one
        `mem_interface.data_bitwidth` memory location. The `memory_mapped_io`
        should use the register information in the `io_interface` to create
        the registers.

        The `io_interface` is created with an `io_signal_definitions` list.
        Each entry in the list is a dictionary which represents a register.
        The first entry in the `io_signal_definitions` list should be taken as
        register 0 and occupy offset 0 in the `memory_mapped_io` address
        space. The second entry should be taken as register 1 and occupy
        offset 1 in the `memory_mapped_io` address space etc.

        The dictionaries in the `io_signal_definitions` list provide
        information on the register type and the signals which should be
        connected to that register. The register types are read-write,
        write-only and read-only (these register types are explained below).
        The first entry in the register signal list should be connected to
        bit 0 of that register and the second entry should be connected to bit
        1 of that register etc

        Read write (RW) registers
        =========================
        Read write registers can be written via the `mem_interface`. These
        registers (and the io connected to them) will hold the value written
        to them until updated. That value can be read via the `mem_interface`.

        To update a single bit the writing code should perform a read, to get
        the current value, update the required bit in the current value and
        then write it.

        Write only (WO) registers
        =========================
        Write only registers can be written via the `mem_interface`. These
        registers (and the io connected to them) will pulse the written value
        for one cycle before returning to 0. A read of write only register
        will always return 0.

        Read only (RO) registers
        ========================
        Read only registers are updated by the IO signals on the
        `io_interface`. These registers can be read via the `mem_interface`.
        Any writes to these registers will not update the register.

        Reading and writing
        ===================
        When `mem_interface.rw_enable` goes high the `memory_mapped_io` should
        check the `mem_interface.w_strobe` signal. If `mem_interface.w_strobe`
        is 0 then it should perform a read. If `mem_interface.w_strobe` is
        not 0 then it should perform a write.

        In both the read and write case the `memory_mapped_io` should set
        `mem_interface.rw_complete` high for one cycle when it detects
        a high on `mem_interface.rw_enable`. It should then set
        `mem_interface.rw_complete` low for at least one cycle. This gives the
        reading/writing block time to receive and respond to the pulse on
        `mem_interface.rw_complete`.

        Read
        ====
        For a read, the `memory_mapped_io` should use `mem_interface.rw_addr`
        to index into the address space and return the data from the address
        over the `mem_interface.r_data` signal.

        Write
        =====
        For a write, the `memory_mapped_io` should use the
        `mem_interface.rw_addr` as the write location. The
        `mem_interface.w_strobe` signal is an n hot signal which specifies
        which bytes in the `mem_interface.w_data` should be written to memory.
        If bit n in the `mem_interface.w_strobe` is high then byte n in the
        `mem_interface.w_data` should be written to byte n in the memory. If
        bit n in the `mem_interface.w_strobe` is low then byte n in the memory
        should be unchanged.
        '''

        cycles = 20000
        n_tests = 500

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.memory_mapped_io_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, memory_mapped_io, memory_mapped_io, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_addresses_outside_register_space(self):
        ''' The `memory_mapped_io` owns `mem_interface.n_bytes` of the address
        space. However the number of registers specified on the `io_interface`
        may not require that entire address space. In this situation the
        `memory_mapped_io` may received valid read and write requests to
        addresses which do not have any IO attached to them. The
        `memory_mapped_io` should respond as if the read or write was valid.
        In the read case, it should return 0. In the write case it should not
        update any addresses.
        '''

        cycles = 20000
        n_tests = 500

        # Set up a mem_interface with n_bytes to make sure the DUT owns plenty
        # of addresses outside of the register address space.
        self.args['mem_interface'] = (
            generate_mem_interface(offset=0, n_bytes=128))

        # Set up an io_interface with 1 register so that most read/write
        # transactions are outside the register address space
        n_reg = 1
        self.args['io_interface'], self.arg_types['io_interface'] = (
            generate_io_interface(
                min_n_registers=n_reg, n_registers_upper_bound=n_reg+1))

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.memory_mapped_io_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, memory_mapped_io, memory_mapped_io, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_registers_with_single_signal(self):
        ''' The memory_mapped_io should work correctly with registers which
        are connected one IO signal.
        '''

        cycles = 20000
        n_tests = 500

        n_signals = 1

        # Calculate the upper bound of n registers which the mem_interface
        # allows
        mem_interface_n_bytes = self.args['mem_interface'].n_bytes
        mem_interface_data_bytewidth = (
            self.args['mem_interface'].data_bytewidth)
        n_registers_upper_bound = (
            mem_interface_n_bytes//mem_interface_data_bytewidth + 1)

        # Create an io_interface with 1 signal on all registers
        self.args['io_interface'], self.arg_types['io_interface'] = (
            generate_io_interface(
                n_registers_upper_bound=n_registers_upper_bound,
                min_n_signals_per_reg=n_signals,
                n_signals_per_reg_upper_bound=n_signals+1))

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.memory_mapped_io_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, memory_mapped_io, memory_mapped_io, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_registers_with_max_n_signals(self):
        ''' The memory_mapped_io should work correctly with registers which
        have every bit connected to IO signals.
        '''

        cycles = 20000
        n_tests = 500

        n_signals = self.args['mem_interface'].data_bitwidth

        # Calculate the upper bound of n registers which the mem_interface
        # allows
        mem_interface_n_bytes = self.args['mem_interface'].n_bytes
        mem_interface_data_bytewidth = (
            self.args['mem_interface'].data_bytewidth)
        n_registers_upper_bound = (
            mem_interface_n_bytes//mem_interface_data_bytewidth + 1)

        # Create an io_interface with 1 signal on all registers
        self.args['io_interface'], self.arg_types['io_interface'] = (
            generate_io_interface(
                n_registers_upper_bound=n_registers_upper_bound,
                min_n_signals_per_reg=n_signals,
                n_signals_per_reg_upper_bound=n_signals+1))

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.memory_mapped_io_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, memory_mapped_io, memory_mapped_io, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_single_register(self):
        ''' The `memory_mapped_io` should function correctly when there is
        just one register specified on the `io_interface`.
        '''

        cycles = 20000
        n_tests = 500

        n_reg = 1
        self.args['io_interface'], self.arg_types['io_interface'] = (
            generate_io_interface(
                min_n_registers=n_reg, n_registers_upper_bound=n_reg+1))

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.memory_mapped_io_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, memory_mapped_io, memory_mapped_io, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_full_address_space(self):
        ''' The `memory_mapped_io` should function correctly when the
        registers fill the `memory_mapped_io` address space.
        '''

        cycles = 20000
        n_tests = 500

        # Pick a random number of bytes
        n_bytes = 1 << random.randrange(3, 6)

        # Set up a mem_interface with n_bytes
        self.args['mem_interface'] = (
            generate_mem_interface(offset=0, n_bytes=n_bytes))

        # Calculate how many registers are required to fill the address space
        mem_interface_data_bytewidth = (
            self.args['mem_interface'].data_bytewidth)
        n_registers = n_bytes//mem_interface_data_bytewidth

        self.args['io_interface'], self.arg_types['io_interface'] = (
            generate_io_interface(
                min_n_registers=n_registers,
                n_registers_upper_bound=n_registers+1))

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.memory_mapped_io_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, memory_mapped_io, memory_mapped_io, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_back_to_back_reads_and_writes(self):
        ''' The `memory_mapped_io` should function correctly when back to back
        reads from and writes to different addresses are requested.
        '''

        cycles = 20000
        n_tests = 500

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(
                self.memory_mapped_io_check(
                    hold_rw_enable_high=True, **kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, memory_mapped_io, memory_mapped_io, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

class TestMemoryMappedIOVivadoVhdl(
    VivadoVHDLTestCase, TestMemoryMappedIO):
    pass

class TestMemoryMappedIOVivadoVerilog(
    VivadoVerilogTestCase, TestMemoryMappedIO):
    pass
