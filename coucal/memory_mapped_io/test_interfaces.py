import random

from kea.test_utils import KeaTestCase as TestCase

from coucal.test_utils import random_string_generator

from .interfaces import IOInterface, AVAILABLE_IO_TYPES

def random_register_dict():
    ''' Generates a random register dictionary.
    '''

    n_signals = random.randrange(1, 10)

    register_dict = {
        'type': random.choice(AVAILABLE_IO_TYPES),
        'signals': [random_string_generator() for n in range(n_signals)]}

    return register_dict

def random_io_signal_definitions_generator(n_registers):
    ''' Generates a random io_signal_definitions list.
    '''

    io_signal_definitions = [
        random_register_dict() for n in range(n_registers)]

    return io_signal_definitions

class TestInterfaces(TestCase):

    def setUp(self):
        pass

    def test_available_io_types(self):
        ''' The `AVAILABLE_IO_TYPES` should be `rw`, `ro`, and `wo`.
        '''

        assert(AVAILABLE_IO_TYPES == ['rw', 'ro', 'wo'])

    def test_zero_registers(self):
        ''' The `IOInterface` should raise an error if `io_signal_definitions`
        is an empty list.
        '''

        invalid_io_signal_definitions = []

        self.assertRaisesRegex(
            ValueError,
            ('IOInterface: io_signal_definitions should contain at least '
             'one register.'),
            IOInterface,
            invalid_io_signal_definitions
        )

    def test_invalid_io_type(self):
        ''' The `IOSignalDefinition` should raise an error if `io_type` is not
        in the `AVAILABLE_IO_TYPES` list.
        '''

        # Generate io_signal_definitions
        n_registers = random.randrange(1, 10)
        invalid_io_signal_definitions = (
            random_io_signal_definitions_generator(n_registers))

        # Overwrite the type of one register with an invalid type
        n = random.choice(range(n_registers))
        invalid_io_type = random_string_generator()
        invalid_io_signal_definitions[n]['type'] = invalid_io_type

        self.assertRaisesRegex(
            ValueError,
            ('IOInterface: Register type should be one of ' +
             ', '.join(AVAILABLE_IO_TYPES) + '. Requested type is ' +
             invalid_io_type + '.'),
            IOInterface,
            invalid_io_signal_definitions
        )

    def test_zero_signals_on_register(self):
        ''' The `IOInterface` should raise an error if `io_signal_definitions`
        contains a register definition with an empty signal list.
        '''

        # Generate io_signal_definitions
        n_registers = random.randrange(1, 10)
        invalid_io_signal_definitions = (
            random_io_signal_definitions_generator(n_registers))

        # Overwrite the signals of one register with an empty list
        n = random.choice(range(n_registers))
        invalid_io_signal_definitions[n]['signals'] = []

        self.assertRaisesRegex(
            ValueError,
            ('IOInterface: All registers should have at least one signal.'),
            IOInterface,
            invalid_io_signal_definitions
        )

    def test_repeated_name(self):
        ''' The `IOInterface` should raise an error if `io_signal_definitions`
        contains two signals with the same name.
        '''

        # Generate io_signal_definitions
        n_registers = random.randrange(2, 10)
        invalid_io_signal_definitions = (
            random_io_signal_definitions_generator(n_registers))

        # Extract one signal name from one register
        n = random.randrange(n_registers)
        repeated_name = (
            random.choice(invalid_io_signal_definitions[n]['signals']))

        # Add that signal name to another register
        m = random.randrange(n_registers)
        invalid_io_signal_definitions[m]['signals'].append(repeated_name)

        self.assertRaisesRegex(
            ValueError,
            ('IOInterface: IO signal names should be unique.'),
            IOInterface,
            invalid_io_signal_definitions
        )
