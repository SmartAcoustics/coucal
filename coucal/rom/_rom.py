from math import log

from myhdl import block, Signal, intbv, always, enum

from coucal.router import PeripheralInterface

@block
def rom(clock, rom_interface, data):
    ''' A read only memory populated with the `data`. This block is designed
    to work with the router. The router will drive the
    `rom_interface.rw_enable` signal correctly as determined by the routing
    bits of the `rom_interface.rw_addr` signal. If this block is not used with
    the router then it may not function correctly as it only checks enough
    bits of the `rom_interface.rw_addr` to get the offset within the ROM.

    The `data` argument should be a list of bytes. If `data` is smaller than
    the `rom_interface.n_bytes` then the remaining bytes will be filled with
    0.
    '''

    if not isinstance(rom_interface, PeripheralInterface):
        raise TypeError(
            'ROM: The rom_interface should be an instance of '
            'PeripheralInterface.')

    data_n_bytes = len(data)
    rom_n_bytes = rom_interface.n_bytes

    if data_n_bytes > rom_n_bytes:
        raise ValueError(
            'ROM: The data is too large for the number of bytes assigned to '
            'this peripheral. The data is ' + hex(data_n_bytes) + ' bytes '
            'and this peripheral owns ' + hex(rom_n_bytes) + ' bytes.')

    # Extract the signals from the rom_interface
    rw_enable = rom_interface.rw_enable
    rw_complete = rom_interface.rw_complete
    rw_addr = rom_interface.rw_addr
    r_data = rom_interface.r_data

    # Get the data bitwidth and bytewidth of the r_data
    r_data_bitwidth = rom_interface.data_bitwidth
    r_data_bytewidth = rom_interface.data_bytewidth

    # Sanity checks: These should be checked in the PeripheralsInterface which
    # generates the rom_interface but we include sanity checks here to make
    # sure
    #
    # Check to make sure the r_data width is a multiple of 8
    assert(r_data_bitwidth % 8 == 0)
    # Check n_bytes is a multiple of the r_data byte width
    assert(rom_n_bytes % r_data_bytewidth == 0)
    # Check n_bytes is a power of 2
    assert(rom_n_bytes & (rom_n_bytes-1) == 0)

    return_objects = []

    # w_data and w_strobe are unused. Set read to True to suppress the
    # conversion warning that signal is driven but not read.
    rom_interface.w_data.read = True
    rom_interface.w_strobe.read = True

    # Calculate the memory depth in words which are r_data_bytewidth
    memory_depth = rom_n_bytes//r_data_bytewidth

    if memory_depth == 1:
        # We have a single memory location so we can only ever read from that
        mem_addr = 0
        # rw_addr is unused in this scenario. Set read on rw_addr to True to
        # suppress the conversion warning that signal is driven but not read.
        rw_addr.read = True

    else:
        # Calculate the lower index to convert from byte address to word
        # address
        mem_addr_lower_index = int(log(r_data_bytewidth)/log(2))

        # Extract the memory address from the rw_addr
        mem_addr = (
            rw_addr(
                rom_interface.n_memory_addressing_bits,
                mem_addr_lower_index))

    data_words = []

    # Reshape the data into a list of words which are r_data_bytewidth
    for n in range(0, data_n_bytes, r_data_bytewidth):
        word = 0

        for m in range(r_data_bytewidth):
            try:
                # Extract the byte from the data
                val = data[n + m]

            except IndexError:
                # We have run out of data but we still need to finish the word
                # so set the val to 0
                val = 0

            if val >= 2**8:
                raise ValueError('ROM: data should be a list of bytes.')

            # Combine the bytes into words
            word |= (val << (8*m))

        data_words.append(word)

    # Pad data_words with zeroes to the memory_depth
    data_words += [0] * (memory_depth - len(data_words))

    # Create the memory
    memory = tuple(data_words)

    t_state = enum('AWAIT_READ', 'PROPAGATION')
    state = Signal(t_state.AWAIT_READ)

    @always(clock.posedge)
    def control():

        if state == t_state.AWAIT_READ:
            if rw_enable:
                # Pulse the rw_complete
                rw_complete.next = True
                state.next = t_state.PROPAGATION

        elif state == t_state.PROPAGATION:
            # Give the core a cycle to receive the data and move on
            rw_complete.next = False
            state.next = t_state.AWAIT_READ

    return_objects.append(control)

    @always(clock.posedge)
    def reader():

        if rw_enable:
            # Output the data from the specified mem_addr
            r_data.next = memory[mem_addr]

    return_objects.append(reader)

    return return_objects
