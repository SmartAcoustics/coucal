import random

from math import log

from myhdl import Signal, block, always, intbv, StopSimulation, enum

from kea.test_utils import KeaTestCase as TestCase
from kea.test_utils import KeaVivadoVHDLTestCase as VivadoVHDLTestCase
from kea.test_utils import KeaVivadoVerilogTestCase as VivadoVerilogTestCase

from coucal.router import (
    PeripheralsInterface, PeripheralInterface, random_address_map_generator)

from ._rom import rom

def generate_rom_interface(offset=None, n_bytes=None):
    ''' Generate the rom_interface.
    '''

    n_peripherals = 1

    # Set an upper bound on the ROM n_bytes so the tests don't take too long
    # to run
    n_bytes_upper_bound = 2**14 + 1

    zero_one_offset = bool(random.randrange(2))

    # Create a random address map
    address_map = (
        random_address_map_generator(
            n_peripherals, n_bytes_upper_bound, zero_one_offset))

    # The address should contain a single peripheral as we only need one for
    # peripheral interface for these tests.
    assert(len(address_map) == 1)

    if offset is not None:
        # Update the address map with the specified offset
        peripheral_name = list(address_map.keys())[0]
        address_map[peripheral_name]['offset'] = offset

    if n_bytes is not None:
        # Update the address map with the specified n_bytes
        peripheral_name = list(address_map.keys())[0]
        address_map[peripheral_name]['n_bytes'] = n_bytes

    # Create the peripherals interface
    peripherals_interface = PeripheralsInterface(address_map)

    peripheral_name = peripherals_interface.peripherals[0]
    rom_interface = (
        peripherals_interface.peripheral_interface_packager(peripheral_name))

    return rom_interface

def test_args_setup():
    ''' Generate the arguments and argument types for the DUT.
    '''

    clock = Signal(False)

    rom_interface = generate_rom_interface()

    n_data_bytes_upper_bound = rom_interface.n_bytes + 1

    n_data_bytes = random.randrange(1, n_data_bytes_upper_bound)
    data = [random.randrange(2**8) for n in range(n_data_bytes)]

    args = {
        'clock': clock,
        'rom_interface': rom_interface,
        'data': data,
    }

    rom_interface_types = {
        'rw_enable': 'custom',
        'rw_complete': 'output',
        'rw_addr': 'custom',
        'w_strobe': 'custom',
        'w_data': 'custom',
        'r_data': 'output',
    }

    arg_types = {
        'clock': 'clock',
        'rom_interface': rom_interface_types,
        'data': 'non-signal',
    }

    return args, arg_types

class TestRomInterface(TestCase):
    ''' The DUT should reject incompatible interfaces and arguments.
    '''

    def setUp(self):

        self.args, _arg_types = test_args_setup()

    def test_invalid_rom_interface(self):
        ''' The `rom` should raise an error if the `rom_interface`
        is not an instance of `PeripheralInterface`.
        '''

        self.args['rom_interface'] = random.randrange(0, 100)

        self.assertRaisesRegex(
            TypeError,
            ('ROM: The rom_interface should be an instance of '
             'PeripheralInterface.'),
            rom,
            **self.args
        )

    def test_invalid_data_length(self):
        ''' The `rom` should raise an error if the `data` list is too big for
        address space asssigned to the ROM.
        '''

        rom_interface = generate_rom_interface()

        peripheral_n_bytes = rom_interface.n_bytes
        data_n_bytes = (
            random.randrange(peripheral_n_bytes+1, peripheral_n_bytes+500))

        self.args['rom_interface'] = rom_interface
        self.args['data'] = [0 for n in range(data_n_bytes)]

        self.assertRaisesRegex(
            ValueError,
            ('ROM: The data is too large for the number of bytes assigned to '
            'this peripheral. The data is ' + hex(data_n_bytes) + ' bytes '
             'and this peripheral owns ' + hex(peripheral_n_bytes) +
             ' bytes.'),
            rom,
            **self.args
        )

    def test_invalid_data_val(self):
        ''' The `rom` should raise an error if the `data` contains a value
        which is larger than a byte.
        '''

        invalid_val_index = random.randrange(len(self.args['data']))
        self.args['data'][invalid_val_index] = random.randrange(2**8, 2**16)

        self.assertRaisesRegex(
            ValueError,
            ('ROM: data should be a list of bytes.'),
            rom,
            **self.args
        )

class TestRom(TestCase):

    def setUp(self):

        self.args, self.arg_types = test_args_setup()

        self.test_count = 0
        self.tests_run = False

    @block
    def end_tests(self, n_tests, **kwargs):

        clock = kwargs['clock']

        return_objects = []

        @always(clock.posedge)
        def control():

            if self.test_count >= n_tests:
                self.tests_run = True
                raise StopSimulation

        return_objects.append(control)

        return return_objects

    def rom_random_stim_values(
        self, w_strobe_upper_bound, w_data_upper_bound, rom_offset,
        rom_addr_space_upper_bound, rw_addr_upper_bound):
        ''' This function generates stim values for the ROM input signals.
        '''

        rom_addr_space_max = rom_addr_space_upper_bound - 1
        rw_addr_max_val = rw_addr_upper_bound - 1

        w_strobe_val = random.randrange(w_strobe_upper_bound)
        w_data_val = random.randrange(w_data_upper_bound)

        random_val = random.random()

        if random_val < 0.05:
            # Set rw_addr to 0 five per cent of the time
            rw_addr_val = 0

        elif random_val < 0.1:
            # Set rw_addr to its max value five per cent of the time
            rw_addr_val = rw_addr_max_val

        elif random_val < 0.15:
            # Set rw_addr to the ROM offset five per cent of the time
            rw_addr_val = rom_offset

        elif random_val < 0.2:
            # Set rw_addr to the highest address in the ROM address space
            # five per cent of the time
            rw_addr_val = rom_addr_space_max

        elif random_val < 0.25 and (
            rom_addr_space_upper_bound < rw_addr_upper_bound):
            # If the next address after the ROM address space is a valid
            # address then set rw_addr to it five per cent of the time.
            rw_addr_val = rom_addr_space_upper_bound

        elif random_val < 0.5:
            # Randomly drive rw_addr twenty five per cent of the time
            rw_addr_val = random.randrange(rw_addr_upper_bound)

        else:
            # Select a random value in the ROM address space fifty per
            # cent of the time.
            rw_addr_val = (
                random.randrange(rom_offset, rom_addr_space_upper_bound))

        return w_strobe_val, w_data_val, rw_addr_val


    @block
    def rom_interface_stim(self, clock, rom_interface, hold_rw_enable_high):
        ''' This block randomly drives the rom_interface.
        '''

        assert(isinstance(rom_interface, PeripheralInterface))

        return_objects = []

        rom_offset = rom_interface.offset
        rom_n_bytes = rom_interface.n_bytes

        rom_addr_space_upper_bound = rom_offset + rom_n_bytes
        rw_addr_upper_bound = 2**len(rom_interface.rw_addr)

        w_strobe_upper_bound = 2**len(rom_interface.w_strobe)
        w_data_upper_bound = 2**len(rom_interface.w_data)

        t_state = enum('STIM', 'AWAIT_COMPLETE')
        state = Signal(t_state.STIM)

        @always(clock.posedge)
        def stim():

            if state == t_state.STIM:
                # Generate random stim values
                w_strobe_val, w_data_val, rw_addr_val = (
                    self.rom_random_stim_values(
                        w_strobe_upper_bound, w_data_upper_bound,
                        rom_offset, rom_addr_space_upper_bound,
                        rw_addr_upper_bound))

                # Whilst rw_enable is low keep updating the other stim signals
                # with random values.
                rom_interface.rw_addr.next = rw_addr_val
                rom_interface.w_strobe.next = w_strobe_val
                rom_interface.w_data.next = w_data_val

                if random.random() < 0.2:
                    # Randomly set rw_enable
                    rom_interface.rw_enable.next = True
                    state.next = t_state.AWAIT_COMPLETE

            elif state == t_state.AWAIT_COMPLETE:
                if rom_interface.rw_complete:
                    # Generate random stim values
                    w_strobe_val, w_data_val, rw_addr_val = (
                        self.rom_random_stim_values(
                            w_strobe_upper_bound, w_data_upper_bound,
                            rom_offset, rom_addr_space_upper_bound,
                            rw_addr_upper_bound))

                    rom_interface.rw_addr.next = rw_addr_val
                    rom_interface.w_strobe.next = w_strobe_val
                    rom_interface.w_data.next = w_data_val

                    if hold_rw_enable_high or random.random() < 0.1:
                        # Keep rw_enable high and set up another read
                        rom_interface.rw_enable.next = True

                    else:
                        rom_interface.rw_enable.next = False
                        state.next = t_state.STIM

        return_objects.append(stim)

        return return_objects

    @block
    def rom_check(self, hold_rw_enable_high=False, **kwargs):

        clock = kwargs['clock']
        rom_interface = kwargs['rom_interface']
        data = kwargs['data']

        assert(isinstance(rom_interface, PeripheralInterface))

        return_objects = []

        return_objects.append(
            self.rom_interface_stim(
                clock, rom_interface, hold_rw_enable_high))

        r_data_bitwidth = rom_interface.data_bitwidth
        r_data_bytewidth = rom_interface.data_bytewidth

        addr_upper_bit = rom_interface.n_memory_addressing_bits

        expected_rw_complete = Signal(False)
        expected_r_data = Signal(intbv(0)[r_data_bitwidth:])

        @always(clock.posedge)
        def check():

            assert(rom_interface.rw_complete == expected_rw_complete)
            assert(rom_interface.r_data == expected_r_data)

            # The ROM should set rw_complete in response to rw_enable. If
            # rw_complete is high then the ROM should wait one cycle for the
            # response to propagate.
            expected_rw_complete.next = (
                rom_interface.rw_enable and not rom_interface.rw_complete)

            if rom_interface.rw_enable:
                # Extract the offset into the ROM address space
                offset = rom_interface.rw_addr[addr_upper_bit:]

                # We need to mask out the lower address bits so that the
                # offset is word aligned
                offset &= ~(r_data_bytewidth-1)

                mem_val = 0

                for n in range(r_data_bytewidth):
                    try:
                        # Shift each memory location into the correct offset
                        # in the expected data
                        mem_val |= data[offset + n] << n*8

                    except IndexError:
                        pass

                expected_r_data.next = mem_val

            if expected_rw_complete:
                self.test_count += 1

        return_objects.append(check)

        return return_objects

    def test_rom(self):
        ''' The ROM should use `data` to populate a region of read only memory
        of length `rom_interface.n_bytes`.

        The `data` argument should be a list of bytes. If the data is not
        large enough to fill `rom_interface.n_bytes` then the remainder of the
        ROM should be populated with zeroes.

        When `rom_interface.rw_enable` goes high the ROM should use the
        `rom_interface.rw_addr` to index into the address space and return the
        data stored in that location. It should also set
        `rom_interface.rw_complete` high for one cycle. The
        `rom_interface.rw_complete` should always go low for at least one
        cycle after going high. This gives the reading block time to receive
        the data and (if required) set up another read.

        The `rom_interface.rw_addr` is addressing a byte in the address space.
        When the `rom_interface.r_data` is wider than a single byte, the ROM
        should read n addresses and fill the `rom_interface.r_data` where n is
        the byte width of `rom_interface.r_data`.

        The ROM is designed to work with a `router` which only sets
        `rom_interface.rw_enable` when the `rom_interface.rw_addr` is within
        the address space of the ROM. For this reason the ROM does not need to
        use the entire `rom_interface.rw_addr` signal. It only needs to use
        enough bits to index into the `rom_interface.n_bytes`. This means that
        the ROM will return data whenever `rom_interface.rw_enable` is high
        even if the upper bits of `rom_interface.rw_addr` mean the address is
        not within the address space of the ROM.

        The ROM should ignore the `rom_interface.w_strobe` and
        `rom_interface.w_data` signals.
        '''

        cycles = 10000
        n_tests = 1000

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.rom_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, rom, rom, self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_min_n_bytes(self):
        ''' The ROM should function correctly when `rom_interface.n_bytes` is
        equal to the byte width of `rom_interface.r_data`. Note this is the
        minimum permitted `rom_interface.n_bytes`.
        '''

        cycles = 10000
        n_tests = 100

        n_peripherals = 1

        # Set the n_bytes to the r_data byte width
        n_bytes = len(self.args['rom_interface'].r_data)//8
        offset = (
            random.randrange(
                0, 2**len(self.args['rom_interface'].rw_addr), n_bytes))

        self.args['rom_interface'] = generate_rom_interface(offset, n_bytes)

        n_data_bytes = random.randrange(1, n_bytes + 1)
        self.args['data'] = [
            random.randrange(2**8) for n in range(n_data_bytes)]

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.rom_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, rom, rom, self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_offset_of_zero(self):
        ''' The ROM should function correctly when `rom_interface.offset` is
        zero.
        '''

        cycles = 10000
        n_tests = 300

        offset = 0
        n_bytes = 16

        self.args['rom_interface'] = generate_rom_interface(offset, n_bytes)
        self.args['data'] = [random.randrange(2**8) for n in range(n_bytes)]

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.rom_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, rom, rom, self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_max_address(self):
        ''' The ROM should function correctly when the ROM address space
        includes the highest address that the `rom_interface.rw_addr` can
        specify.
        '''

        cycles = 10000
        n_tests = 300

        n_bytes = 16
        offset = 2**len(self.args['rom_interface'].rw_addr) - n_bytes

        self.args['rom_interface'] = generate_rom_interface(offset, n_bytes)
        self.args['data'] = [random.randrange(2**8) for n in range(n_bytes)]

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.rom_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, rom, rom, self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_empty_data(self):
        ''' The ROM should function correctly when the `data` is empty. All
        addresses should read 0.
        '''

        cycles = 10000
        n_tests = 1000

        self.args['data'] = []

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.rom_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, rom, rom, self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_one_byte_data(self):
        ''' The ROM should function correctly when the `data` contains a
        single value. In this case only the least significant byte in offset
        zero contains data. All other addresses should read 0.
        '''

        cycles = 10000
        n_tests = 1000

        self.args['data'] = [random.randrange(2**8) for n in range(1)]

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.rom_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, rom, rom, self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_data_fills_address_space(self):
        ''' The ROM should function correctly when the `data` fills the
        address space of the ROM.
        '''

        cycles = 10000
        n_tests = 1000

        n_bytes = self.args['rom_interface'].n_bytes
        self.args['data'] = [random.randrange(2**8) for n in range(n_bytes)]

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.rom_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, rom, rom, self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_back_to_back_reads(self):
        ''' The ROM should function correctly when back to back reads of
        different addresses are requested.
        '''

        cycles = 10000
        n_tests = 1000

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(
                self.rom_check(hold_rw_enable_high=True, **kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, rom, rom, self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

class TestRomVivadoVhdl(VivadoVHDLTestCase, TestRom):
    pass

class TestRomVivadoVerilog(VivadoVerilogTestCase, TestRom):
    pass
