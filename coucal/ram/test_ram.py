import random

from myhdl import Signal, block, always, intbv, StopSimulation, enum

from kea.test_utils import KeaTestCase as TestCase
from kea.test_utils import KeaVivadoVHDLTestCase as VivadoVHDLTestCase
from kea.test_utils import KeaVivadoVerilogTestCase as VivadoVerilogTestCase

from coucal.router import (
    PeripheralsInterface, PeripheralInterface, random_address_map_generator)

from ._ram import ram

def generate_ram_interface(offset=None, n_bytes=None):
    ''' Generate the ram_interface.
    '''

    n_peripherals = 1

    # Set an upper bound on the RAM n_bytes so the tests don't take too long
    # to run
    n_bytes_upper_bound = 2**14 + 1

    zero_one_offset = bool(random.randrange(2))

    # Create a random address map
    address_map = (
        random_address_map_generator(
            n_peripherals, n_bytes_upper_bound, zero_one_offset))

    # The address should contain a single peripheral as we only need one for
    # peripheral interface for these tests.
    assert(len(address_map) == 1)

    if offset is not None:
        # Update the address map with the specified offset
        peripheral_name = list(address_map.keys())[0]
        address_map[peripheral_name]['offset'] = offset

    if n_bytes is not None:
        # Update the address map with the specified n_bytes
        peripheral_name = list(address_map.keys())[0]
        address_map[peripheral_name]['n_bytes'] = n_bytes

    # Create the peripherals interface
    peripherals_interface = PeripheralsInterface(address_map)

    peripheral_name = peripherals_interface.peripherals[0]
    ram_interface = (
        peripherals_interface.peripheral_interface_packager(peripheral_name))

    return ram_interface

def test_args_setup():
    ''' Generate the arguments and argument types for the DUT.
    '''

    clock = Signal(False)

    ram_interface = generate_ram_interface()

    args = {
        'clock': clock,
        'ram_interface': ram_interface,
    }

    ram_interface_types = {
        'rw_enable': 'custom',
        'rw_complete': 'output',
        'rw_addr': 'custom',
        'w_strobe': 'custom',
        'w_data': 'custom',
        'r_data': 'output',
    }

    arg_types = {
        'clock': 'clock',
        'ram_interface': ram_interface_types,
    }

    return args, arg_types

class TestRamInterface(TestCase):
    ''' The DUT should reject incompatible interfaces and arguments.
    '''

    def setUp(self):

        self.args, _arg_types = test_args_setup()

    def test_invalid_ram_interface(self):
        ''' The `ram` should raise an error if the `ram_interface`
        is not an instance of `PeripheralInterface`.
        '''

        self.args['ram_interface'] = random.randrange(0, 100)

        self.assertRaisesRegex(
            TypeError,
            ('RAM: The ram_interface should be an instance of '
             'PeripheralInterface.'),
            ram,
            **self.args
        )

class TestRam(TestCase):

    def setUp(self):

        self.args, self.arg_types = test_args_setup()

        self.test_count = 0
        self.tests_run = False

    @block
    def end_tests(self, n_tests, **kwargs):

        clock = kwargs['clock']

        return_objects = []

        @always(clock.posedge)
        def control():

            if self.test_count >= n_tests:
                self.tests_run = True
                raise StopSimulation

        return_objects.append(control)

        return return_objects

    def ram_random_stim_values(
        self, w_strobe_upper_bound, w_data_upper_bound, ram_offset,
        ram_addr_space_upper_bound, rw_addr_upper_bound):
        ''' This function generates stim values for the RAM input signals.
        '''

        ram_addr_space_max = ram_addr_space_upper_bound - 1
        rw_addr_max_val = rw_addr_upper_bound - 1

        w_data_val = random.randrange(w_data_upper_bound)

        if bool(random.randrange(2)):
            # Half the time set w_strobe to 0 so the RAM performs a read
            w_strobe_val = 0

        else:
            # Half the time set w_strobe to a non 0 value so the RAM
            # performs a write
            w_strobe_val = random.randrange(1, w_strobe_upper_bound)

        random_val = random.random()

        if random_val < 0.05:
            # Set rw_addr to 0 five per cent of the time
            rw_addr_val = 0

        elif random_val < 0.1:
            # Set rw_addr to its max value five per cent of the time
            rw_addr_val = rw_addr_max_val

        elif random_val < 0.15:
            # Set rw_addr to the RAM offset five per cent of the time
            rw_addr_val = ram_offset

        elif random_val < 0.2:
            # Set rw_addr to the highest address in the RAM address space
            # five per cent of the time
            rw_addr_val = ram_addr_space_max

        elif random_val < 0.25 and (
            ram_addr_space_upper_bound < rw_addr_upper_bound):
            # If the next address after the RAM address space is a valid
            # address then set rw_addr to it five per cent of the time.
            rw_addr_val = ram_addr_space_upper_bound

        elif random_val < 0.5:
            # Randomly drive rw_addr twenty five per cent of the time
            rw_addr_val = random.randrange(rw_addr_upper_bound)

        else:
            # Select a random value in the RAM address space fifty per
            # cent of the time.
            rw_addr_val = (
                random.randrange(ram_offset, ram_addr_space_upper_bound))

        return w_strobe_val, w_data_val, rw_addr_val

    @block
    def ram_interface_stim(self, clock, ram_interface, hold_rw_enable_high):
        ''' This block randomly drives the ram_interface.
        '''

        assert(isinstance(ram_interface, PeripheralInterface))

        return_objects = []

        ram_offset = ram_interface.offset
        ram_n_bytes = ram_interface.n_bytes

        ram_addr_space_upper_bound = ram_offset + ram_n_bytes
        rw_addr_upper_bound = 2**len(ram_interface.rw_addr)

        w_strobe_upper_bound = 2**len(ram_interface.w_strobe)
        w_data_upper_bound = 2**len(ram_interface.w_data)

        t_state = enum('STIM', 'AWAIT_COMPLETE')
        state = Signal(t_state.STIM)

        @always(clock.posedge)
        def stim():

            if state == t_state.STIM:
                # Generate random stim values
                w_strobe_val, w_data_val, rw_addr_val = (
                    self.ram_random_stim_values(
                        w_strobe_upper_bound, w_data_upper_bound,
                        ram_offset, ram_addr_space_upper_bound,
                        rw_addr_upper_bound))

                # Whilst rw_enable is low keep updating the other stim signals
                # with random values.
                ram_interface.rw_addr.next = rw_addr_val
                ram_interface.w_strobe.next = w_strobe_val
                ram_interface.w_data.next = w_data_val

                if random.random() < 0.2:
                    # Randomly set rw_enable
                    ram_interface.rw_enable.next = True
                    state.next = t_state.AWAIT_COMPLETE

            elif state == t_state.AWAIT_COMPLETE:
                if ram_interface.rw_complete:
                    # Generate random stim values
                    w_strobe_val, w_data_val, rw_addr_val = (
                        self.ram_random_stim_values(
                            w_strobe_upper_bound, w_data_upper_bound,
                            ram_offset, ram_addr_space_upper_bound,
                            rw_addr_upper_bound))

                    ram_interface.rw_addr.next = rw_addr_val
                    ram_interface.w_strobe.next = w_strobe_val
                    ram_interface.w_data.next = w_data_val

                    if hold_rw_enable_high or random.random() < 0.1:
                        # Keep rw_enable high and set up another read
                        ram_interface.rw_enable.next = True

                    else:
                        ram_interface.rw_enable.next = False
                        state.next = t_state.STIM

        return_objects.append(stim)

        return return_objects

    @block
    def ram_check(self, hold_rw_enable_high=False, **kwargs):

        clock = kwargs['clock']
        ram_interface = kwargs['ram_interface']

        assert(isinstance(ram_interface, PeripheralInterface))

        return_objects = []

        return_objects.append(
            self.ram_interface_stim(
                clock, ram_interface, hold_rw_enable_high))

        assert(len(ram_interface.r_data) == len(ram_interface.w_data))

        data_bitwidth = ram_interface.data_bitwidth
        data_bytewidth = ram_interface.data_bytewidth

        addr_upper_bit = ram_interface.n_memory_addressing_bits

        expected_rw_complete = Signal(False)
        expected_r_data = Signal(intbv(0)[data_bitwidth:])

        memory = [Signal(intbv(0)[8:]) for n in range(ram_interface.n_bytes)]

        @always(clock.posedge)
        def check():

            assert(ram_interface.rw_complete == expected_rw_complete)
            assert(ram_interface.r_data == expected_r_data)

            # The RAM should set rw_complete in response to rw_enable. If
            # rw_complete is high then the RAM should wait one cycle for the
            # response to propagate.
            expected_rw_complete.next = (
                ram_interface.rw_enable and not ram_interface.rw_complete)

            if ram_interface.rw_enable:
                # Extract the offset into the RAM address space
                offset = ram_interface.rw_addr[addr_upper_bit:]

                # We need to mask out the lower address bits so that the
                # offset is word aligned
                offset &= ~(data_bytewidth-1)

                if ram_interface.w_strobe == 0:
                    # A read in progress
                    mem_val = 0

                    for n in range(data_bytewidth):
                        # Shift each memory location into the correct offset
                        # in the expected data
                        mem_val |= memory[offset + n] << n*8

                    expected_r_data.next = mem_val

                else:
                    # A write is in progress
                    for n in range(data_bytewidth):
                        if ram_interface.w_strobe[n]:
                            memory[offset + n].next = (
                                ram_interface.w_data[8*(n+1):8*n])

            if expected_rw_complete:
                self.test_count += 1

        return_objects.append(check)

        return return_objects

    def test_ram(self):
        ''' The RAM should contain a memory of length `ram_interface.n_bytes`.

        When `ram_interface.rw_enable` goes high the RAM should check the
        `ram_interface.w_strobe` signal. If `ram_interface.w_strobe` is 0 then
        the RAM should perform a read. If `ram_interface.w_strobe` is not 0
        then the RAM should perform a write.

        Read
        ====
        For a read, the RAM should use `ram_interface.rw_addr` to index into
        the address space and return the data from the address over the
        `ram_interface.r_data` signal.

        Write
        =====
        For a write, the RAM should use the `ram_interface.rw_addr` as the
        write location. The `ram_interface.w_strobe` signal is an n hot signal
        which specifies which bytes in the `ram_interface.w_data` should be
        written to memory. If bit n in the `ram_interface.w_strobe` is high
        then byte n in the `ram_interface.w_data` should be written to byte n
        in the memory. If bit n in the `ram_interface.w_strobe` is low then
        byte n in the memory should be unchanged.

        In both the read and write case the RAM should set
        `ram_interface.rw_complete` high for one cycle when it detects
        a high on `ram_interface.rw_enable`. It should then set
        `ram_interface.rw_complete` low for at least one cycle. This gives the
        reading/writing block time to receive and respond to the pulse on
        `ram_interface.rw_complete`.

        The `ram_interface.rw_addr` is addressing a byte in the address space.
        When the `ram_interface.r_data` and `ram_interface.w_data` are wider
        than a single byte, the RAM should read from or write to n addresses
        where n is the data byte width.

        The RAM is designed to work with a `router` which only sets
        `ram_interface.rw_enable` when the `ram_interface.rw_addr` is within
        the address space of the RAM. For this reason the RAM does not need to
        use the entire `ram_interface.rw_addr` signal. It only needs to use
        enough bits to index into the `ram_interface.n_bytes`. This means that
        the RAM will return data whenever `ram_interface.rw_enable` is high
        even if the upper bits of `ram_interface.rw_addr` mean the address is
        not within the address space of the RAM.
        '''

        cycles = 20000
        n_tests = 2000

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.ram_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, ram, ram, self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_min_n_bytes(self):
        ''' The RAM should function correctly when `ram_interface.n_bytes` is
        equal to the byte width of `ram_interface.r_data`. Note this is the
        minimum permitted `ram_interface.n_bytes`.
        '''

        cycles = 20000
        n_tests = 100

        n_peripherals = 1

        # Set the n_bytes to the r_data byte width
        n_bytes = len(self.args['ram_interface'].r_data)//8
        offset = (
            random.randrange(
                0, 2**len(self.args['ram_interface'].rw_addr), n_bytes))

        self.args['ram_interface'] = generate_ram_interface(offset, n_bytes)

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.ram_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, ram, ram, self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_offset_of_zero(self):
        ''' The RAM should function correctly when `ram_interface.offset` is
        zero.
        '''

        cycles = 20000
        n_tests = 300

        offset = 0
        n_bytes = 16

        self.args['ram_interface'] = generate_ram_interface(offset, n_bytes)

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.ram_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, ram, ram, self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_max_address(self):
        ''' The RAM should function correctly when the RAM address space
        includes the highest address that the `ram_interface.rw_addr` can
        specify.
        '''

        cycles = 20000
        n_tests = 300

        n_bytes = 16
        offset = 2**len(self.args['ram_interface'].rw_addr) - n_bytes

        self.args['ram_interface'] = generate_ram_interface(offset, n_bytes)

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.ram_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, ram, ram, self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_back_to_back_reads_and_writes(self):
        ''' The RAM should function correctly when back to back reads from and
        writes to different addresses are requested.
        '''

        cycles = 20000
        n_tests = 2000

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(
                self.ram_check(hold_rw_enable_high=True, **kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, ram, ram, self.args, self.arg_types,
            custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

class TestRamVivadoVhdl(VivadoVHDLTestCase, TestRam):
    pass

class TestRamVivadoVerilog(VivadoVerilogTestCase, TestRam):
    pass
