import copy

from myhdl import block

from coucal.core import (
    CoreMemoryInterface, picorv32_wrapper, copy_picorv32_verilog_src)
from coucal.memory_mapped_peripherals import memory_mapped_peripherals

@block
def core_and_peripherals(
    clock, n_reset, trap, interrupts, address_map, core_parameters,
    rom_data=None, io_interface=None, axil_interface=None):
    ''' This block instantiates the picorv32 wrapper and the memory mapped
    peripherals and connects them up.
    '''

    return_objects = []

    # Create the core_memory_interface
    core_memory_interface = CoreMemoryInterface()

    core_args = {
        'clock': clock,
        'n_reset': n_reset,
        'trap': trap,
        'core_memory_interface': core_memory_interface,
        'interrupts': interrupts,
    }

    # Add the core parameters to the core args
    core_args.update(core_parameters)

    # Create the PicoRV32
    return_objects.append(picorv32_wrapper(**core_args))

    # Create the memory mapped peripherals
    return_objects.append(
        memory_mapped_peripherals(
            clock, core_memory_interface, address_map, rom_data=rom_data,
            io_interface=io_interface, axil_interface=axil_interface))

    return return_objects

class CoreAndPeripherals(object):
    ''' This class instantiates the picorv32 wrapper and the memory mapped
    peripherals and connects them up. It also provides a method for converting
    the core and peripherals which copies the picorv32 source verilog into the
    correct location.
    '''

    def __init__(
        self, clock, n_reset, trap, interrupts, address_map, core_parameters,
        rom_data=None, io_interface=None, axil_interface=None):
        ''' Creates the core_and_peripherals block.
        '''

        self.core_and_peripherals = (
            core_and_peripherals(
                clock, n_reset, trap, interrupts, address_map,
                core_parameters, rom_data=rom_data, io_interface=io_interface,
                axil_interface=axil_interface))

    def convert(self, **kwargs):
        ''' Converts the core_and_peripherals block and copies the PicoRV32
        source code to the required directory.
        '''

        if kwargs['hdl'] != 'Verilog':
            raise ValueError(
                'PicoRV32Core conversion: The requested HDL is invalid. We '
                'currently only convert to Verilog.')

        # Convert the core_and_peripherals. The conversion process consumes
        # the entries in the dict so we take a copy.
        conversion_args = copy.deepcopy(kwargs)
        self.core_and_peripherals.convert(**conversion_args)

        try:
            destination_directory = kwargs['path']
        except KeyError:
            destination_directory = '.'

        # Copy the picorv32 verilog source file to the destination
        copy_picorv32_verilog_src(destination_directory)
