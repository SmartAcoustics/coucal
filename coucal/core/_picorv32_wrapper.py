import os
import shutil

from pathlib import Path
from myhdl import block, always

from .interfaces import (
    CoreMemoryInterface, ADDR_BITWIDTH, DATA_BITWIDTH, W_STROBE_BITWIDTH,
    INTERRUPT_BITWIDTH)

def copy_picorv32_verilog_src(destination_directory):

    # Create the destination directory if it doesn't exist
    Path(destination_directory).mkdir(parents=True, exist_ok=True)

    # Combine the directory of this file with 'picorv32.v' to get the PicoRV32
    # source filepath
    picorv32_source_filepath = (
        os.path.join(Path(__file__).parent.absolute(), 'picorv32.v'))

    # Copy the PicoRV32 source file to the destination
    shutil.copy(picorv32_source_filepath, destination_directory)

def bool_to_integer_str(boolean):
    ''' If boolean is True then a '1' is returned.

    If boolean is False than a '0' is returned.
    '''

    assert(isinstance(boolean, bool))

    if boolean:
        return '1'
    else:
        return '0'

coucal_picorv32_core_count = 0

@block
def picorv32_wrapper(
    clock, n_reset, trap, core_memory_interface, interrupts, stack_address,
    program_reset_address, program_interrupt_address,
    enable_compressed_instructions, enable_interrupts, interrupts_mask,
    interrupts_latches, enable_interrupts_q_registers,
    enable_timer_interrupt):
    ''' A block to instantiate a PicoRV32 core.

    NOTE: The picorv32.v source file will need to be included in the build
    system for this block to work.
    '''

    addr_upper_bound = 2**ADDR_BITWIDTH
    interrupt_upper_bound = 2**INTERRUPT_BITWIDTH

    # Check that the core_memody_interface is of the correct type
    assert(isinstance(core_memory_interface, CoreMemoryInterface))

    # Check the bitwidths of the signals on the core_memory_interface
    assert(len(core_memory_interface.rw_addr) == ADDR_BITWIDTH)
    assert(len(core_memory_interface.w_data) == DATA_BITWIDTH)
    assert(len(core_memory_interface.r_data) == DATA_BITWIDTH)
    assert(len(core_memory_interface.w_strobe) == W_STROBE_BITWIDTH)

    # Check the interrupts bitwidth
    assert(len(interrupts) == INTERRUPT_BITWIDTH)

    # Check that the stack_address is a positive number which is less than the
    # address upper bound and is 16 byte aligned.
    assert(stack_address < addr_upper_bound)
    assert(stack_address >= 0)
    assert(stack_address % 16 == 0)

    # Check that the program_reset_address is a positive number which is less
    # than the address upper bound.
    assert(program_reset_address < addr_upper_bound)
    assert(program_reset_address >= 0)

    # Check that the program_interrupt_address is a positive number which is
    # less than the address upper bound.
    assert(program_interrupt_address < addr_upper_bound)
    assert(program_interrupt_address >= 0)

    # Check enable_compressed_instructions is a boolean
    assert(isinstance(enable_compressed_instructions, bool))

    # Check enable_interrupts is a boolean
    assert(isinstance(enable_interrupts, bool))

    # Check that the interrupts_mask is a positive number which is less than
    # the interrupt upper bound.
    assert(interrupts_mask < interrupt_upper_bound)
    assert(interrupts_mask >= 0)

    # Check that the interrupts_latches is a positive number which is less
    # than the interrupt upper bound.
    assert(interrupts_latches < interrupt_upper_bound)
    assert(interrupts_latches >= 0)

    # Check enable_interrupts_q_registers is a boolean
    assert(isinstance(enable_interrupts_q_registers, bool))

    # Check enable_timer_interrupt is a boolean
    assert(isinstance(enable_timer_interrupt, bool))

    if enable_timer_interrupt:
        # If enable timer interrupt is set low then check the timer interrupt
        # (bit zero) of the interrupts signal is not masked out (low in
        # interrupt mask enables the interrupt and high disables the
        # interrupt).
        assert((interrupts_mask & 1) == 0)

    global coucal_picorv32_core_count

    # Convert the input parameters to strings
    stack_address_str = str(stack_address)
    program_reset_address_str = str(program_reset_address)
    program_interrupt_address_str = str(program_interrupt_address)
    enable_compressed_instructions_str = (
        bool_to_integer_str(enable_compressed_instructions))
    enable_interrupts_str = bool_to_integer_str(enable_interrupts)
    interrupts_mask_str = str(interrupts_mask)
    interrupts_latches_str = str(interrupts_latches)
    enable_interrupts_q_registers_str = (
        bool_to_integer_str(enable_interrupts_q_registers))
    enable_timer_interrupt_str = bool_to_integer_str(enable_timer_interrupt)

    # Extract the signals from the core_memory_interface
    mem_valid = core_memory_interface.rw_enable
    mem_ready = core_memory_interface.rw_complete
    mem_addr = core_memory_interface.rw_addr
    mem_wdata = core_memory_interface.w_data
    mem_wstrb = core_memory_interface.w_strobe
    mem_rdata = core_memory_interface.r_data

    # Need to specify if the signals are inputs or outputs for the conversion
    clock.read = True
    n_reset.read = True
    trap.driven = 'wire'

    mem_valid.driven = 'wire'
    mem_ready.read = True
    mem_addr.driven = 'wire'
    mem_wdata.driven = 'wire'
    mem_wstrb.driven = 'wire'
    mem_rdata.read = True

    interrupts.read = True

    # Increment the instantiation count
    inst_count = coucal_picorv32_core_count
    coucal_picorv32_core_count += 1

    # Behavioural model for Myhdl simulations
    # =======================================
    @always(clock.posedge)
    def behavioural_model():
        # We do not currently have a behavioural model of the core. We still
        # include this function so we have something to return.
        pass

    # Verilog instantiation for conversion
    # ====================================
    picorv32_wrapper.verilog_code = """
        // PicoRV32 core

        picorv32 #(
            .STACKADDR($stack_address_str),
            .PROGADDR_RESET($program_reset_address_str),
            .PROGADDR_IRQ($program_interrupt_address_str),
            .COMPRESSED_ISA($enable_compressed_instructions_str),
            .ENABLE_IRQ($enable_interrupts_str),
            .MASKED_IRQ($interrupts_mask_str),
            .LATCHED_IRQ($interrupts_latches_str),
            .ENABLE_IRQ_QREGS($enable_interrupts_q_registers_str),
            .ENABLE_IRQ_TIMER($enable_timer_interrupt_str)
            )
        picorv32_$inst_count (
            .clk($clock),
            .resetn($n_reset),
            .trap($trap),
            .mem_valid($mem_valid),
            .mem_instr(),             // Not connected
            .mem_ready($mem_ready),
            .mem_addr($mem_addr),
            .mem_wdata($mem_wdata),
            .mem_wstrb($mem_wstrb),
            .mem_rdata($mem_rdata),
            .mem_la_read(),           // Not connected
            .mem_la_write(),          // Not connected
            .mem_la_addr(),           // Not connected
            .mem_la_wdata(),          // Not connected
            .mem_la_wstrb(),          // Not connected
            .pcpi_valid(),            // Not connected
            .pcpi_insn(),             // Not connected
            .pcpi_rs1(),              // Not connected
            .pcpi_rs2(),              // Not connected
            .pcpi_wr(1'b0),           // Held low
            .pcpi_rd(32'h 0000_0000), // Held at 0
            .pcpi_wait(1'b0),         // Held low
            .pcpi_ready(1'b0),        // Held low
            .irq($interrupts),
            .eoi(),                   // Not connected
            .trace_valid(),           // Not connected
            .trace_data()             // Not connected
            );

        // End of picorv32 instantiation
        """

    # VHDL instantiation for conversion
    # =================================
    # NOT YET IMPLEMENTED

    return behavioural_model
