import copy

from .interfaces import CoreMemoryInterface
from ._picorv32_wrapper import picorv32_wrapper, copy_picorv32_verilog_src

class PicoRV32Core(object):
    ''' This class instantiates the picorv32 wrapper. It also provides a
    method for converting the core which copies the picorv32 source verilog
    into the correct location.
    '''

    def __init__(
        self, clock, n_reset, trap, core_memory_interface, interrupts,
        core_parameters):
        ''' Creates the picorv32_wrapper block.
        '''

        args = {
            'clock': clock,
            'n_reset': n_reset,
            'trap': trap,
            'core_memory_interface': core_memory_interface,
            'interrupts': interrupts,
        }

        # Add the core parameters to the args
        args.update(core_parameters)

        self.core = picorv32_wrapper(**args)

    def convert(self, **kwargs):
        ''' Converts the core block and copies the PicoRV32 source code to the
        required directory.
        '''

        if kwargs['hdl'] != 'Verilog':
            raise ValueError(
                'PicoRV32Core conversion: The requested HDL is invalid. We '
                'currently only convert to Verilog.')

        # Convert the core. The conversion process consumes the entries in the
        # dict so we take a copy.
        conversion_args = copy.deepcopy(kwargs)
        self.core.convert(**conversion_args)

        try:
            destination_directory = kwargs['path']
        except KeyError:
            destination_directory = '.'

        # Copy the picorv32 verilog source file to the destination
        copy_picorv32_verilog_src(destination_directory)
