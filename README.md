# Coucal

FPGA support blocks for the PicoRV32 softcore.

This repository also contains examples for building an FPGA containing a
PicoRV32 and examples for compiling rust to run on the PicoRV32.

## Virtual environment setup
To create the virtual environment for converting the HDL run:

``` python
virtualenv coucal_venv
```

Then activate the virtual environment:
```python
source coucal_venv/bin/activate
```

Then install the requirements:
```python
pip3 install -r requirements.txt
```

## Examples
This repo contains an example for building rust for the PicoRV32 and for
building the FPGA binary.

If you want to set up the FPGA projects in the examples then clone this
directory with:
```
$ git clone git@gitlab.com:toby.gomersall/coucal.git
```
