use std::env;
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

use strict_yaml_rust::StrictYamlLoader;

/// Put the linker script somewhere the linker can find it.
fn main() {

    // Configure the build
    configure_build();

    let out_dir = env::var("OUT_DIR").expect("No out dir");
    let dest_path = Path::new(&out_dir);

    println!("cargo:rustc-link-search={}", dest_path.display());

    println!("cargo:rerun-if-changed=memory.x");
    println!("cargo:rerun-if-changed=build.rs");
}

fn configure_build() {
    // Read in the system_config.yaml and use the values from it to configure
    // the build. This function will write out memory.x.

    let filename = "system_config.yaml";

    // Read the system config yaml in to a string
    let mut file = File::open(filename)
        .expect("Unable to open the system configuration file");
    let mut buffer = String::new();
    file.read_to_string(&mut buffer)
        .expect("Unable to read the system configuration");

    // Interpret the yaml string
    let configs = StrictYamlLoader::load_from_str(&buffer).unwrap();
    let config = &configs[0];

    // Extract the required values from the address_map
    let rom_offset = str_to_int(
        config["address_map"]["rom"]["offset"].as_str().unwrap());
    let rom_n_bytes = str_to_int(
        config["address_map"]["rom"]["n_bytes"].as_str().unwrap());
    let ram_offset = str_to_int(
        config["address_map"]["ram"]["offset"].as_str().unwrap());
    let ram_n_bytes = str_to_int(
        config["address_map"]["ram"]["n_bytes"].as_str().unwrap());

    // Extract the required values from the core_parameters
    let stack_address = str_to_int(
        config["core_parameters"]["stack_address"].as_str().unwrap());
    let program_interrupt_address = str_to_int(
        config["core_parameters"]["program_interrupt_address"]
        .as_str().unwrap());
    let program_reset_address = str_to_int(
        config["core_parameters"]["program_reset_address"].as_str().unwrap());

    // The stack pointer to be set to an address within the address space of
    // the RAM. We could set the stack address to any value in the RAM
    // as long as there is enough space below the start address for the stack
    // to grow down.
    //
    // The standard calling convention for RISC V means the stack pointer
    // should point to the last word in use. Addresses lower than the current
    // value of the stack pointer are free (ie programs using the stack
    // pointer will first decrement the stack pointer). This means we can set
    // the stack address to the upper bound of the RAM address space.
    if stack_address > (ram_offset + ram_n_bytes) ||
        stack_address < ram_offset {
            panic!(
                "Stack address should be within the RAM address space. Note: \
                the stack address can be set to the RAM upper bound.");
        }

    if program_interrupt_address >= (rom_offset + rom_n_bytes) ||
        program_interrupt_address < rom_offset {
            panic!(
                "Program interrupt address should be within the ROM address \
                space.");
        }

    if program_reset_address >= (rom_offset + rom_n_bytes) ||
        program_reset_address < rom_offset {
            panic!(
                "Program reset address should be within the ROM address \
                space.");
        }

    // Set up the string to write to the memory.x
    let data = format!(
        "MEMORY\n\
        {{\n  \
        ROM (rx) : ORIGIN = {}, LENGTH = {}\n  \
        RAM (rw) : ORIGIN = {}, LENGTH = {}\n\
        }}\n\
        \n\
        REGION_ALIAS(\"FLASH\", ROM);\n\
        \n\
        PROVIDE(STACK_ADDRESS = {});\n\
        PROVIDE(PROGRAM_RESET_ADDRESS = {});\n\
        PROVIDE(PROGRAM_INTERRUPT_ADDRESS = {});",
        rom_offset, rom_n_bytes, ram_offset, ram_n_bytes, stack_address,
        program_reset_address, program_interrupt_address);

    // Write the memory.x file
    fs::write("memory.x", data).expect("Unable to write file");

    let out_dir = env::var("OUT_DIR").expect("No out dir");
    let dest_path = Path::new(&out_dir);
    fs::copy("memory.x", &dest_path.join("memory.x"))
        .expect("Unable to write file");
}

fn str_to_int(integer_string: &str) -> u32 {
    // Convert a string to an integer. If the string starts with '0x' then
    // this function will parse the string as a hex value otherwise it will
    // treat it as a decimal.
    let integer =
        if integer_string.starts_with("0x") {
            let without_prefix = integer_string.trim_start_matches("0x");
            u32::from_str_radix(without_prefix, 16).unwrap()
        }
        else {
            u32::from_str_radix(integer_string, 10).unwrap()
        };

    integer
}
