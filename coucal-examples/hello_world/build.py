import argparse
import subprocess
import os
import tempfile
import shutil
import tarfile

from string import Template
from pathlib import Path

from fpga_src.hdl_conversion import fpga_conversion

PROJECT_NAME = 'hello_world'

def compile_sw():
    ''' Compile the software.
    '''

    clean_process = subprocess.Popen(['cargo', 'clean'])

    out, err = clean_process.communicate()

    if clean_process.returncode != 0:
        raise RuntimeError('Software clean up failed.')

    compile_process = subprocess.Popen(['cargo', 'build', '--release'])

    out, err = compile_process.communicate()

    if compile_process.returncode != 0:
        raise RuntimeError('Software compilation failed.')

def compile_and_import_sw():
    ''' Convert the compiled software object to a list of bytes.
    '''

    compile_sw()

    with tempfile.TemporaryDirectory() as tmpdirname:
        # Generate the temporary file path
        temp_file_path = (
            os.path.join(tmpdirname, 'converted_sw.bin'))

        # Convert the compiled software objects to a binary file
        convert_process = subprocess.Popen([
            'cargo', 'objcopy', '--release', '--', '-O', 'binary',
            temp_file_path])

        out, err = convert_process.communicate()

        if convert_process.returncode != 0:
            raise RuntimeError('Software conversion to binary failed.')

        rom_data = []

        with open(temp_file_path, "rb") as f:
            # Read the first byte out of the binary file
            byte = f.read(1)
            while byte != b"":
                # Add the byte to the rom_data
                rom_data.append(byte[0])
                # Read the next byte out of the binary file
                byte = f.read(1)

    return rom_data

def populate_template(template_file, output_file, template_substitutions):
    # Read in the template file
    with open(template_file, 'r') as f_template:
        template = Template(f_template.read())

    # Substitute the variables into the template
    output = template.safe_substitute(template_substitutions)

    # Check that the directories in the output_file path exist. If not,
    # create them.
    os.makedirs(os.path.dirname(output_file), exist_ok=True)

    # Write the output file.
    with open(output_file, 'w+') as f_output:
        f_output.write(output)

def assemble_project(create_pl_tcl):
    ''' Assemble the FPGA sources into a project.
    '''

    # Create the command to run the file
    run_file_command = (
        'vivado -mode batch -nolog -nojournal -source ' + create_pl_tcl)

    run_process = subprocess.Popen(
        run_file_command.split(), stdin=subprocess.PIPE,
        stdout=out_pipe, stderr=err_pipe, universal_newlines=True)

    out, err = run_process.communicate()

    if run_process.returncode != 0:
        raise RuntimeError(
            "vivado failed assembling {ftr}: {err}"
            .format(ftr=create_pl_tcl, err=err))

if __name__=='__main__':

    ####################
    # Argument parsing #
    ####################

    build_sw = 'build_sw'
    convert_fpga = 'convert_fpga'
    assemble_fpga_project = 'assemble_fpga_project'
    generate_fpga_binary = 'generate_fpga_binary'
    build_all_fpga = 'build_all_fpga'

    build_options = [
        build_sw, convert_fpga, assemble_fpga_project, generate_fpga_binary,
        build_all_fpga]

    arg_parser = argparse.ArgumentParser()

    arg_parser.add_argument(
        "build_option",
        action="store",
        choices=build_options,
        help="Select which part of the system to build.")

    args = arg_parser.parse_args()
    build_option = args.build_option

    ###############
    # Definitions #
    ###############

    system_config_yaml_path = 'system_config.yaml'

    fpga_src_directory = 'fpga_src'
    create_pl_template = 'create_pl.tcl.template'
    system_bd_template = 'system_bd.tcl.template'
    generate_bin_template = 'generate_binary.tcl.template'
    src_files = [
        os.path.join(fpga_src_directory, 'constraints.xdc'),
        os.path.join(fpga_src_directory, 'system_tb.v'),
        os.path.join(fpga_src_directory, 'system_tb_behav.wcfg'),]

    bd_design_name = 'system_bd'
    bd_design_wrapper_name = bd_design_name + '_wrapper'

    fpga_build_directory = 'fpga_build'
    fpga_project_directory = (
        os.path.join(fpga_build_directory, 'build'))
    fpga_build_src_directory = os.path.join(fpga_build_directory, 'src')

    fpga_project_binary_path = (
        os.path.join(
            fpga_project_directory, PROJECT_NAME + '.runs', 'impl_1',
            bd_design_wrapper_name + '.bin'))
    fpga_output_binary = os.path.join(fpga_build_directory, 'bin', 'fpga.bin')

    # Generate the destination directory for the create_pl template. Remove
    # the .template suffix.
    template_suffix = '.template'

    # Create the destination file path for the create_pl tcl script
    assert(create_pl_template.endswith(template_suffix))
    create_pl_tcl = os.path.join(
        fpga_build_src_directory, create_pl_template[:-len(template_suffix)])

    # Create the destination file path for the system_bd tcl script
    assert(system_bd_template.endswith(template_suffix))
    system_bd_tcl = os.path.join(
        fpga_build_src_directory, system_bd_template[:-len(template_suffix)])

    # Create the destination file path for the generate_bin tcl script
    assert(generate_bin_template.endswith(template_suffix))
    generate_bin_tcl = os.path.join(
        fpga_build_src_directory,
        generate_bin_template[:-len(template_suffix)])

    out_pipe = subprocess.PIPE
    err_pipe = subprocess.PIPE

    #########
    # Build #
    #########

    if build_option == build_sw:
        # Compile the software
        compile_sw()

    if build_option == convert_fpga or build_option == build_all_fpga:

        # Create the directories for the build source
        Path(fpga_build_src_directory).mkdir(parents=True, exist_ok=True)

        # At the moment we populate the ROM with just the software
        rom_data = compile_and_import_sw()

        # Convert the FPGA
        fpga_conversion(
            fpga_build_src_directory, system_config_yaml_path, rom_data)

        template_substitutions = {
            'project_name': PROJECT_NAME,
            'bd_design_name': bd_design_name,
            'bd_design_wrapper_name': bd_design_wrapper_name,
            'fpga_project_directory': fpga_project_directory,
            'fpga_build_src_directory': fpga_build_src_directory,}

        # Populate the create pl template and copy it to the correct location
        create_pl_template_path = (
            os.path.join(fpga_src_directory, create_pl_template))
        populate_template(
            create_pl_template_path, create_pl_tcl, template_substitutions)

        # Populate the system_bd template and copy it to the correct location
        system_bd_template_path = (
            os.path.join(fpga_src_directory, system_bd_template))
        populate_template(
            system_bd_template_path, system_bd_tcl, template_substitutions)

        # Populate the generate bin template and copy it to the correct
        # location
        generate_bin_template_path = (
            os.path.join(fpga_src_directory, generate_bin_template))
        populate_template(
            generate_bin_template_path, generate_bin_tcl,
            template_substitutions)

        for src_file in src_files:
            # Copy the fpga source files to the build source directory
            shutil.copy(src_file, fpga_build_src_directory)

    if (build_option == assemble_fpga_project or
        build_option == build_all_fpga):

        # If the build directory exists from a previous build, archive it
        previous_build_archive = (
            os.path.join(fpga_build_directory, 'previous_build.tar.gz'))
        try:
            with tarfile.open(previous_build_archive, "w:gz") as tar:
                tar.add(
                    fpga_project_directory, arcname=os.path.basename('build'))
                shutil.rmtree(fpga_project_directory)
        except OSError:
            pass

        # Assemble the FPGA
        assemble_project(create_pl_tcl)

    if (build_option == generate_fpga_binary or
        build_option == build_all_fpga):

        # Set the command to generate the binary
        generate_binary = (
            'vivado -mode batch -nolog -nojournal -source ' +
            generate_bin_tcl)

        # Generate the FPGA binary
        run_process = subprocess.Popen(
            generate_binary.split(), stdin=subprocess.PIPE,
            stdout=out_pipe, stderr=err_pipe, universal_newlines=True)

        out, err = run_process.communicate()

        if run_process.returncode != 0:
            raise RuntimeError(
                "vivado failed generating {ftr}: {err}".
                format(ftr=generate_bin_tcl, err=err))

        # Check that the directories in the fpga_output_binary path exist. If
        # not, create them.
        os.makedirs(os.path.dirname(fpga_output_binary), exist_ok=True)

        # Copy the fpga binary to the binary directory
        shutil.copy(fpga_project_binary_path, fpga_output_binary)
