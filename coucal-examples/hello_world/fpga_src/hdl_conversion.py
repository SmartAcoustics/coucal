import os

from strictyaml import load, Map, MapPattern, Str, Int
from myhdl import toVHDL, toVerilog, Signal, intbv

from kea.axi import AxiLiteInterface

from coucal.core import (
    ADDR_BITWIDTH, DATA_BITWIDTH, W_STROBE_BITWIDTH, INTERRUPT_BITWIDTH)
from coucal.core_and_peripherals import CoreAndPeripherals
from coucal.memory_mapped_peripherals import (
    AVAILABLE_PERIPHERALS as COUCAL_AVAILABLE_PERIPHERALS,
    ROM as COUCAL_ROM,
    RAM as COUCAL_RAM,
    MEMORY_MAPPED_IO as COUCAL_MEMORY_MAPPED_IO,
    AXI_LITE_ADAPTER as COUCAL_AXI_LITE_ADAPTER)
from coucal.memory_mapped_io import IOInterface

toVHDL.initial_values = True
toVerilog.initial_values = True

def import_system_config(system_config_yaml_file_path):
    ''' Import the system config yaml specified and extract the address_map
    and core_parameters.
    '''

    # Read in the yaml string from the system config yaml
    with open(system_config_yaml_file_path, 'r') as addr_map_file:
        yaml_string = addr_map_file.read()

    # Extract the data from the yaml string
    yaml_system_config = load(yaml_string).data
    yaml_core_parameters = yaml_system_config['core_parameters']
    yaml_address_map = yaml_system_config['address_map']

    core_parameters = {}
    address_map = {}

    for parameter in yaml_core_parameters:
        # Extract the core parameters from the system config.
        parameter_val = yaml_core_parameters[parameter]

        if parameter_val.lower() in ['true']:
            core_parameters[parameter] = True

        elif parameter_val.lower() in ['false']:
            core_parameters[parameter] = False

        else:
            # Parameter value is not a boolean so interpret it as an integer
            core_parameters[parameter] = int(parameter_val, 0)

    for peripheral in yaml_address_map:
        # For all the coucal peripherals, check that they are valid. We only
        # allow coucal peripherals in this design.
        if peripheral not in COUCAL_AVAILABLE_PERIPHERALS:
            raise ValueError(
                'import_system_config: Invalid peripheral. All peripherals '
                'in the address map section of the system config should be '
                'one of ' + ', '.join(COUCAL_AVAILABLE_PERIPHERALS) + '.')

        # Extract the offset and n_bytes and then add the peripheral to
        # the address map
        address_map[peripheral] = {
            'offset': int(yaml_address_map[peripheral]['offset'], 0),
            'n_bytes': int(yaml_address_map[peripheral]['n_bytes'], 0),
        }

    return core_parameters, address_map

def fpga_conversion(
    destination_path, system_config_yaml_file_path, rom_data=None):
    ''' Convert the FPGA code.
    '''

    conversion_hdl = 'Verilog'

    clock = Signal(False)
    n_reset = Signal(False)
    trap = Signal(False)
    interrupts = Signal(intbv(0)[INTERRUPT_BITWIDTH:])
    core_parameters, address_map = (
        import_system_config(system_config_yaml_file_path))

    args = {
        'clock': clock,
        'n_reset': n_reset,
        'trap': trap,
        'interrupts': interrupts,
        'address_map': address_map,
        'core_parameters': core_parameters,
    }

    if COUCAL_ROM in address_map:
        # There is a ROM in the address map so we need to add the ROM data.
        assert(rom_data is not None)
        args['rom_data'] = rom_data

    if COUCAL_MEMORY_MAPPED_IO in address_map:
        # Create the signal definitions for the memory mapped IO
        io_signal_definitions = [
            {'type': 'rw',
             'signals': ['mmio_rw_0']},
            {'type': 'wo',
             'signals': ['mmio_wo_0']},
            {'type': 'ro',
             'signals': ['mmio_ro_0']},]

        args['io_interface'] = IOInterface(io_signal_definitions)

    if COUCAL_AXI_LITE_ADAPTER in address_map:
        # Create an axi lite interface
        args['axil_interface'] = (
            AxiLiteInterface(
                DATA_BITWIDTH, ADDR_BITWIDTH, use_AWPROT=False,
                use_ARPROT=False, use_WSTRB=True))

    # Create the core and peripherals
    core_and_peripherals = CoreAndPeripherals(**args)

    # Convert the core and peripherals
    core_and_peripherals.convert(
        hdl=conversion_hdl, path=destination_path,
        name='core_and_peripherals')
