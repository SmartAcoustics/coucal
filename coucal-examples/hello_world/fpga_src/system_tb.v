`timescale 1 ns / 1 ps

module system_tb;
	reg fpga_clock = 1;
	always #5 fpga_clock = ~fpga_clock;

	reg resetn = 0;
	initial begin
		if ($test$plusargs("vcd")) begin
			$dumpfile("system.vcd");
			$dumpvars(0, system_tb);
		end
		repeat (1000) @(posedge fpga_clock);
		resetn <= 1;
	end

    wire led1;

	system_bd_wrapper system_bd_wrapper (
		.fpga_clock        (fpga_clock        ),
		.led1(led1)
	);

endmodule
