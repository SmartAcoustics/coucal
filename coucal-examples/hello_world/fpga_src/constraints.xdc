##############################################################################
#                               PACKAGE PINS                                 #
##############################################################################

#-- Clocks
set_property PACKAGE_PIN E3 [get_ports fpga_clock]

#-- Switches
set_property PACKAGE_PIN A8 [get_ports switch1]
set_property PACKAGE_PIN C11 [get_ports switch2]

#-- LEDs
# LED4 = H5
# LED5 = J5
# LED6 = T9
# LED7 = T10
set_property PACKAGE_PIN H5 [get_ports led1]
set_property PACKAGE_PIN J5 [get_ports led2]

##############################################################################
#                               IO STANDARDS                                 #
##############################################################################

#-- Clocks
set_property IOSTANDARD LVCMOS33 [get_ports fpga_clock]

#-- Switches
set_property IOSTANDARD LVCMOS33 [get_ports switch1]
set_property IOSTANDARD LVCMOS33 [get_ports switch2]

#-- LEDs
set_property IOSTANDARD LVCMOS33 [get_ports led1]
set_property IOSTANDARD LVCMOS33 [get_ports led2]

##############################################################################
#                                  TIMING                                    #
##############################################################################

#-- Switches
# Asynchronous but we constrain the signals so the tools don't send the
# signals via a ridiculous path.
set_input_delay -clock [get_clocks -of_objects [get_ports fpga_clock]] -min -add_delay 0 [get_ports switch1]
set_input_delay -clock [get_clocks -of_objects [get_ports fpga_clock]] -max -add_delay 3 [get_ports switch1]

set_input_delay -clock [get_clocks -of_objects [get_ports fpga_clock]] -min -add_delay 0 [get_ports switch2]
set_input_delay -clock [get_clocks -of_objects [get_ports fpga_clock]] -max -add_delay 3 [get_ports switch2]

#-- LEDs
# We constrain the signals so the tools don't send the signals via a
# ridiculous path.
set_output_delay -clock [get_clocks -of_objects [get_ports fpga_clock]] -min -add_delay 0 [get_ports led1]
set_output_delay -clock [get_clocks -of_objects [get_ports fpga_clock]] -max -add_delay 4 [get_ports led1]

set_output_delay -clock [get_clocks -of_objects [get_ports fpga_clock]] -min -add_delay 0 [get_ports led2]
set_output_delay -clock [get_clocks -of_objects [get_ports fpga_clock]] -max -add_delay 4 [get_ports led2]
