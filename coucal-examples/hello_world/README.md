# hello-world
The intent of this example is to demonstrate how to cross compile rust for the
picorv32 core, load it into a ROM (provided by Coucal), set up an FPGA project
and build the FPGA binary. All of these stages are performed by the included
python build script.

This project makes use of the ROM, RAM, memory mapped IO and AXI lite adapter
provided by Coucal.

This repository was set up to compile a simple hello world rust program to run
on a [PicoRV32](https://github.com/YosysHQ/picorv32) core.

The FPGA project has been set up to build a binary to run on the Arty A7
development board.

The simple hello world program exercises standard functions such as reading
instructions from the ROM, using the stack and interrupts. It also exercises
the memory mapped IO and AXI lite adapter provided by Coucal.

## Rust system setup
Rust now supports RISC-V out of the box so there is no need to install an
external toolchain.

Please run the following to install the dependencies:

``` console
$ cargo install cargo-binutils
```
``` console
$ rustup component add llvm-tools-preview
```
``` console
$ rustup target add riscv32imc-unknown-none-elf
```

Other RISC-V targets are available and a list can be found
[here](https://doc.rust-lang.org/nightly/rustc/platform-support.html).

## Rust repository setup
A few additional files are required so that it builds correctly.

### Config
This repo includes a `.cargo/config.toml` with a `build` section:
```
[build]
target = "riscv32imc-unknown-none-elf"
```
This section specifies the default target so that when we call `cargo build`
it correctly builds for the target system.

The next section contains rust flags for a particular target:
```
[target.riscv32imc-unknown-none-elf]
rustflags = [
  "-C", "link-arg=-Tlink.x",
]
```
This tells the linker to use the `link.x` linker script when building for
`riscv32imc-unknown-none-elf`.

### Linker scripts
The repo must now provide the linker scripts specified in
`.cargo/config.toml`.

#### link.x
The linker script is provided by
[coucal-rt](https://github.com/tobygomersall/coucal-rt). This repo was
forked from
[picorv32-rt](https://github.com/ilya-epifanov/picorv32-rt) and modified.

The [coucal-rt](https://github.com/tobygomersall/coucal-rt) version of
`link.x` has been modified in the following ways:

1. The `.heap` and `.stack` sections to have been updated to be `NOLOAD`. I
copied this strategy from the
[riscv-rt](https://docs.rs/riscv-rt/latest/riscv_rt/) crate. In the original
version the `.heap` and `.stack` sections are set to be `INFO`. For some
reason this means the linker requires the RAM to be at offset zero (it errors
if not).
2. The coucal-rt version of `link.x` uses the `STACK_START_ADDRESS`,
`PROGRAM_RESET_ADDRESS` and `PROGRAM_INTERRUPT_ADDRESS` to position code in
the compiled output. Those addresses are also passed to the PicoRV32 core when
it is converted. This modification to `link.x` means we are using the same
addresses for the core conversion and for the software compilation. These 
values must be provided by `memory.x`.

The `link.x` requires that the following variables are set in `memory.x`:
1. `STACK_START_ADDRESS`
2. `PROGRAM_INTERRUPT_ADDRESS`
3. `PROGRAM_RESET_ADDRESS`
4. `FLASH` (`ROM`) `ORIGIN` and 'LENGTH
5. `RAM` `ORIGIN` and `LENGTH`

#### memory.x
The `link.x` linker script requires a `memory.x` to provide the required
variables. The [build script](#build-script) reads in the
`system_config.yaml`, extracts the relevant information and writes the
`memory.x`. Note: The `memory.x` will only appear after running the build.

### Build script
The build script (`build.rs`) reads in the `system_config.yaml`, extracts the
relevant information and then writes the `memory.x`.

### System configuration - software
For the software, the `system_config.yaml` must contain an address map and the
core parameters.

The FPGA conversion code will also read the `system_config.yaml`. This allows
the converted core and the compiled software to use the same configuration.

#### Address map
The software build system requires that the address map section contains a ROM
and a RAM. The ROM should be called `rom` and the RAM should be called `ram`.

#### Core parameters
The software build system requires that the core parameters sections contains
the `stack_start_address`, `program_interrupt_address` and
`program_reset_address` so they can be included in the `memory.x`

## Software features
The code (`src/main.rs`) contains some unusual elements which are explained
here.

### No std
`#![no_std]` is a crate level attribute that indicates that the crate will
link to the core crate instead of the std crate. See
[The Embedonomicon](https://docs.rust-embedded.org/embedonomicon/smallest-no-std.html)
for more information.

### No main
The `#![no_main]` attribute which means that the program won't use the
standard main function as its entry point. See
[The Embedonomicon](https://docs.rust-embedded.org/embedonomicon/smallest-no-std.html)
for more information.

### Panic halt
The system requires a `#[panic_handler]` attribute. The function marked with
this attribute defines the behavior of panics, both library level panics
and language level panics (out of bounds indexing). The
panic-halt crate provides this function.

### entry
The `#[entry]` attribute is provided by the `picorv32-rt` and specifies the
entry point to the program.

### Signature
The entry function is not allowed to return so we make it a divergent
function, which is a function with signature `fn(/* .. */) -> !`.

## Interrupts - software

See the [low level interrupt explanation](#low-level-interrupt-explanation)
for more detail on how the interrupts work.

### Setup interrupts in software
To enable interrupts without Q registers, the  `interrupts` feature should be
enabled on the `picorv32-rt` crate. Ensure interrupts are
[enabled in the FPGA](#interrupts-in-the-fpga).

To enable interrupts with Q registers, the  `interrupts-qregs` feature should
be enabled on the `picorv32-rt` and `picorv32-rs` crates. Ensure interrupts
and Q registers are [enabled in the FPGA](#interrupts-in-the-fpga).

### How to use interrupts
When we enable the `interrupts-qregs` or `interrupts` feature in `picorv32-rt`
it sets up the output binary such that the `.trap` function is placed at
offset `0x10` (this is set in `link.x`). This address should be provided to
the core as parameter `PROGADDR_IRQ`.

When the core receives an interrupt it reads the `PROGADDR_IRQ` address. When
enabled the `.trap` function provided by `picorv32-rt` stores the required
registers and then calls the trap handler.

We can either define our own `trap_handler` or we can include the
`picorv32-rt` trap handler in our code using the `picorv32_interrupts!`
macro. This macro allows us to write our own interrupt handler for each
interrupt. We pass these interrupt handlers to the `picorv32_interrupts!`
macro which sets up the code so that when an interrupt occurs, it checks
which IRQs have interrupted then calls the relevant handlers.

## Building the software
If you modify `link.x` or `memory.x` then you need to clean the build system
before building:
``` console
$ cargo clean
```

To build the release version of the software (release is recommended to
minimize the size of the binary):
``` console
$ cargo build --release
```

## Software behaviour
This code will write 'Hello world!' to the AXI IO. After which it will loop
over the following cycle:

1. Read the AXI IO and memory mapped IO input registers.
2. If bit 0 of the AXI IO input register is high then it will toggle bit 0 of
the AXI IO output register.
3. If bit 0 of the memory mapped IO Input register is high then it will toggle
bit 0 of the memory mapped IO output register.

The outputs will be toggled with a period of 2 seconds (ie 1 second high, 1
second low).

You can confirm the build system is working by running the
[FPGA simulation](#fpga-simulation).

## FPGA system requirements
To build this FPGA binary you will require Vivado. Before running the python
build script you should source vivado (for example):

```
source /opt/Xilinx/Vivado/2022.2/settings64.sh
```

## FPGA build script
The hello world project includes `build.py`. This build script can compile the
software, add it to the ROM, set up the Vivado project and build the FPGA
binary for the hello world project.

### Software compilation
The python build script can be used to compile the software with the following
command:

```
python build.py build_sw
```

This function performs a `cargo clean` followed by a `cargo build --release`.

### Conversion
The python build script can be used to set up the FPGA build source files with
the following command:

```
python build.py convert_fpga
```

As part of the conversion process the build script compiles the software and
adds it to a ROM (if a ROM is included in the address map).

The conversion steps are:

1. Compile the software (there is no need to pre-compile the software).
2. Convert the compiled software output to a binary file.
3. Read in the system configuration from `system_config.yaml`.
4. Add the compiled software to the ROM.
5. Convert the Coucal blocks to HDL (`fpga_src/hdl_conversion.py`).
6. Set up the Vivado build scripts (the `fpga_src/*.template` files).
7. Place all build source files in `fpga_build/src`.

#### System configuration - FPGA
The `fpga_src/hdl_conversion.py` script reads in the `system_config.yaml`. For
the FPGA, the `system_config.yaml` must contain an address map and the core
parameters.

The software build script will also read the `system_config.yaml`. This allows
the converted core and the compiled software to use the same configuration.

##### Address map
The `fpga_src/hdl_conversion.py` script inspects the peripherals included. In
this example project, the `hdl_conversion` script errors if the address map
does not include a `rom` and a `ram`. It will also error if any peripherals in
the address map are not available in the Coucal library. Note: this is
entirely project dependent and a project specific `hdl_conversion` can
implement alternative behaviour (such as using external [non FPGA based]
memory devices).

##### Core parameters
The `fpga_src/hdl_conversion.py` script passes the core parameters to the
Coucal core. All parameters required by the Coucal core should be included in
the core parameters. Any other parameters will be rejected. The required
parameters are:

1. `stack_address` (`STACKADDR`): Set the stack pointer to the highest address
in the RAM so it can grow down. Note that the RISC-V calling convention
requires the stack pointer to be aligned on 16 bytes boundaries. Note: the
standard calling convention for RISC V means the stack pointer should point to
the last word in use and free memory is at addresses lower than the current
value of the stack pointer (ie program using the stack pointer will first
decrement the stack pointer). This means we can set the stack address to the
max RAM address plus one.
2. `program_reset_address` (`PROGADDR_RESET`): Set the start address of the
program in ROM which the core initialise at and jump to when reset. In our
case the program starts at offset zero so we set Progaddr Reset to the ROM
offset.
3. `program_interrupt_address` (`PROGADDR_IRQ`): Set the address of the
instruction in ROM that the core should jump to when interrupted.
4. `enable_compressed_instructions` (`COMPRESSED_ISA`: Set `True` to enable
compressed instructions. The software build makes use of compressed
instructions so we need to enable this functionality in the core.
5. `enable_interrupts` (`ENABLE_IRQ`): Set `True` to enable interrupts. The
software uses interrupts so we need to enable this functionality in the core.
6. `enable_interrupt_q_registers` (`ENABLE_IRQ_QREGS`): Set `True` to enable Q
registers. When enabled the core provides two extra registers for use during
interrupt handling.
7. `enable_timer_interrupt` (`ENABLE_IRQ_TIMER`): Set `True` to enable timer
interrupts.
8. `interrupts_mask` (`MASKED_IRQ`): A compile time setting to enable/disable
registers. A `1` in this bitmask permanently disables the corresponding
interrupt.
9. `interrupts_latches` (`LATCHED_IRQ`): A compile time setting to
enable/disable latching on a particular interrupt. A `1` in this bitmask
enables latching on the corresponding interrupt.

### Project assembly
The python build script can be used to set up the Vivado project with the
following command:

```
python build.py assemble_fpga_project
```

This will create a Vivado project in `fpga_build/build`. It will add the build
sources (from `fpga_build/src`) to the project and create the block diagram.
Once this command has been run, we can open up the Vivado project to inspect
the design, make modifications or run the simulation.

### Binary generation
The python build script can be used to build the FPGA binary with the
following command:

```
python build.py generate_fpga_binary
```

Once the build has completed, the output binary can be found in
`fpga_build/bin`.

Note that the build constraints are provided by `fpga_src/constraints.xdc` and
are copied in to `fpga_build/src` during conversion.

### Build all
The python build script can be used to run conversion, project assembly and
binary generation with the following command:

```
python build.py build_all_fpga
```

## Vivado project
After running [conversion](#conversion) and
[project assembly](#project-assembly) we can open up the Vivado project.

### Block diagram
With the Vivado project open we can inspect the block diagram.

The Vivado block diagram includes the `CoreAndPeripherals` block from Coucal.
This block wraps the PicoRV32 core and the Coucal `memory_mapped_peripherals`.
In this project these peripherals include the ROM (which has been loaded with
the rust software), a RAM for the stack, memory mapped IO and an AXI lite
adapter.

All AXI peripherals which are connected to the memory mapped peripheral AXI
lite interface need to be given an address which is within the address space
of the AXI lite adapter (specified in `address_map.yaml`). These addresses can
be set in the Address Editor tab of the block diagram.

### Interrupts in the FPGA
See the [low level interrupt explanation](#low-level-interrupt-explanation)
for more detail on how the interrupts work.

To enable interrupts in the FPGA, the following core parameters need to be
set in the `system_config.yaml`:
- `enable_interrupts` = true
- `interrupts_mask` (A 1 in this bitmask permanently disables that IRQ)
- `interrupts_latches` (A 1 in this bitmask enables latching on that IRQ)

To enable the timer interrupt set the following:
- `interrupts_mask[0]` = 0
- `enable_timer_interrupt` = true

To enable Q registers set the following:
- `enable_interrupts_q_registers` = 1

Note, the software features need to be [set](#setup-interrupts-in-software)
to match the FPGA settings.

### FPGA simulation
With the Vivado project open we can run the simulation. The simulation sources
(waveform config and `system_tb.v`) are copied in to `fpga_build/src` during
the conversion stage.

1. Click `Run Simulation` in Flow Navigator in Vivado.
2. Then click `Run for 50 us` (play symbol with a T in brackets).
3. Inspect `gpio_io_o[31:0]` and check that it writes "Hello world!".

Additional signals which can be useful when debugging code are:
- The `reg_pc[31:0]` signal in the `picorv32` is the program counter.
- The 32 `cpu_regs[31:0]` are the processor registers (x0, x1, x2 etc). See
the RISC-V Calling Convention section of the
[RISC-V reference card](https://www.cl.cam.ac.uk/teaching/1617/ECAD+Arch/files/docs/RISCVGreenCardv8-20151013.pdf).

#### Debugging interrupts in the FPGA
The interrupt Q registers (when enabled) are `cpuregs[32:36]`.

## Running the FPGA on hardware
After running [conversion](#conversion),
[project-assembly](#project-assembly) and
[binary generation](#binary-generation) we can open up the Vivado project and
program the device with the following sequence:

1. Click `Open Hardware Manager` in Flow Navigator in Vivado.
2. Connect the PS to the board with a JTAG or USB (if an on board JTAG is
present).
3. Click `open target -> Auto connect`.
4. Right click on the FPGA part in the Hardware window.
5. Either:
    1. `Add Configuration Memory Device` ([memory devices](#arty-memory-devices)).
    2. Select the configuration memory device on your board.
    3. `Program device memory`.
    4. Select the bitstream in `fpga_build/bin`.
6. Or (the configuration file will be lost when the board is powered down):
    1. `Program device`.
    2. Select the bitstream in `fpga_build/bin`.

When `SWITCH1` is thrown you should see `LED1` blink repeatedly (1 second on
and 1 second off). Otherwise `LED1` should hold its state.

When `SWITCH2` is thrown you should see `LED2` blink repeatedly (1 second on
and 1 second off). Otherwise `LED2` should hold its state.

### Arty memory devices
See the [arty a7 reference manual](https://digilent.com/reference/programmable-logic/arty-a7/reference-manual) for the quad SPI flash present on the board. In my case the memory
device selected when I `Add Configuration Memory Device` is
`s25fl128sxxxxxx0-spi-x1_x2_x4`.

## Alternative hardware
This project was set up for the Arty-A7. If you are using different hardware
you will need to modify the files in `fpga_src`.

## Software Debug
[cargo-binutils](https://github.com/rust-embedded/cargo-binutils) provide
useful tools for inspecting the compiled executable. A few examples are
included here (see the repository for more information).

To inspect the executable sections in the compiled executable:
``` console
$ cargo objdump --release -- -d
```

To inspect the entire compiled executable:
``` console
$ cargo objdump --release -- -D
```

A [RISC-V Interpreter](https://www.cs.cornell.edu/courses/cs3410/2019sp/riscv/interpreter/)
can help with debugging the output of `cargo objdump`.

This [RISC-V reference card](https://www.cl.cam.ac.uk/teaching/1617/ECAD+Arch/files/docs/RISCVGreenCardv8-20151013.pdf)
is another useful tool for interpreting the output of `cargo objdump`.

[riscv-gnu-toolchain](https://github.com/riscv-collab/riscv-gnu-toolchain)
provides useful tools for inspecting the compiled executable. The advantage of
these tools over the cargo-binutils is that they correct the endianness of the
assembly. This makes it easier to inspect the memory reads in the Vivado
simulation. A few examples are included here but you need to have the
toolchain available on your system (and included in your PATH).

To inspect the executable sections in the compiled executable:
``` console
$ riscv32-unknown-elf-objdump -d /path_to_release_dir/hello-world
```

To inspect the entire compiled executable:
``` console
$ riscv32-unknown-elf-objdump -D /path_to_release_dir/hello-world
```

## Low level interrupt explanation
During compilation, the linker places `.trap` at offset
`program_interrupt_address` [from `system_config.yaml`]. This address is also
passed to the core.

When the core receives an interrupt it reads the `program_interrupt_address`
address (ie it reads the `.trap function`.

`.trap` is provided by `picorv32-rt/src/asm.rs` but it is feature gated such
that:

- If `interrupts-qregs` or `interrupts` are enabled it behaves in the
following way:
    1. It stores the caller saved registers on the stack.
    2. Sets up `a0` with a pointer to the stored registers.
    3. Sets up `a1` with the pending IRQs.
    4. Calls `_start_trap_rust` which takes `a0` as argument 0 and `a1` as
    argument 1. This corresponds to stored registers and IRQ bitmask.
    5. `_start_trap_rust` calls the `trap_handler`.
    6. Restores the registers from the stack.
    7. Returns to the last intruction before the interrupt.

- If interrupts are disabled:
    The core should never interrupt and read `program_interrupt_address`
    therefore the `.trap` function should never be run. If interrupts are not
    enabled in software then they shouldn't be enabled in the FPGA either.

    In this case the `.trap` function just calls `_start`.

## Useful resources
[The Embedonomicon](https://docs.rust-embedded.org/embedonomicon/preface.html)\
[RISC-V Bytes: Rust Cross-Compilation](https://danielmangum.com/posts/risc-v-bytes-rust-cross-compilation/)\
[Stephen Marz's OS block](https://osblog.stephenmarz.com/)\
[Porting RISC-V to Xilinx Kintex 7, Artix 7 and Spartan 7](https://www.irmo.de/2020/06/18/porting-risc-v-to-xilinx-kintex-7-and-spartan-7/)
