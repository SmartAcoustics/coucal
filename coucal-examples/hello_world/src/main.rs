#![no_std]
#![no_main]

use fixedvec::{alloc_stack, FixedVec};
use coucal_rt::{entry, picorv32_interrupts};
use coucal_rs::asm::{maskirq, waitirq, timer};

use panic_halt as _;

fn read_gpio_val(addr: u32) -> u32 {

    unsafe {
        core::ptr::read_volatile(addr as *mut u32)
    }
}

fn write_gpio_val(val: u32, addr: u32) -> () {

    unsafe {
        (addr as *mut u32).write_volatile(val);
    }
}


fn write_gpio_vec(vec: &[u32], addr: u32) -> () {

    for val in vec.iter() {
        // Write each value in vec to the GPIO.
        write_gpio_val(*val, addr);
    }
}

// Pauses for n cycles. Will always pause for at least 128 cycles. This is in
// case ROM reads are slow (eg if the system is using AXI to read from ROM).
// If the ROM reads are slow and n_cycles is too small then the interrupt
// timer could reach zero and interrupt before the core has read the waitirq()
// instruction. This could cause the program to hang as the core waits for an
// interrupt that has already occured.
fn pause(n_cycles: u32) -> () {

    let n_cycles = {
        if n_cycles < 128 {
            128
        }
        else {
            n_cycles
        }
    };

    let _remaining_n_cycles = unsafe { timer(n_cycles) };
    let _pending_interrupts = unsafe { waitirq() };
}

// use `main` as the entry point of this application
// `main` is not allowed to return
#[entry]
fn main() -> ! {

    // NOTE: These must match the address map in system_config.yaml
    const MMIO_BASE_ADDR: u32 = 0x02000000;
    const AXI_IO_BASE_ADDR: u32 = 0x03000000;

    // The output registers for both the MMIO and AXI IO are at offset 0
    // (bytes)
    const MMIO_OUTPUT: u32 = MMIO_BASE_ADDR + 0;
    const AXI_IO_OUTPUT: u32 = AXI_IO_BASE_ADDR + 0;

    // The input registers for both the MMIO and AXI IO are at offset 8
    // (bytes)
    const MMIO_INPUT: u32 = MMIO_BASE_ADDR + 8;
    const AXI_IO_INPUT: u32 = AXI_IO_BASE_ADDR + 8;


    const VECTOR_LENGTH: usize = 62;
    let mut preallocated_space = alloc_stack!([u32; VECTOR_LENGTH]);
    let mut vec = FixedVec::new(&mut preallocated_space);

    // A vector containing the ASCII values for "Hello world!"
    vec.push_all(&[
                 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x77, 0x6F, 0x72, 0x6C,
                 0x64, 0x21]).unwrap();

    // Write vec to the GPIO
    write_gpio_vec(vec.as_slice(), AXI_IO_OUTPUT);

    // Enable just the timer interrupt
    let timer_interrupt_enable_mask: u32 = 0xFFFFFFFE;
    let _old_mask = unsafe { maskirq(timer_interrupt_enable_mask) };

    // The FPGA is running off a 100MHz clock so set pause_n_cycles to
    // 100,000,000 to get a 1 second pause.
    let pause_n_cycles: u32 = 100_000_000;

    loop {

        // Check the input switches
        let mmio_enabled: u32 = read_gpio_val(MMIO_INPUT);
        let axi_io_enabled: u32 = read_gpio_val(AXI_IO_INPUT);

        // Toggle the LEDs
        if (mmio_enabled & 1) == 1 {
            write_gpio_val(0x1, MMIO_OUTPUT);
        }
        if (axi_io_enabled & 1) == 1 {
            write_gpio_val(0x0, AXI_IO_OUTPUT);
        }

        // Pause for pause_n_cycles
        pause(pause_n_cycles);

        // Check the input switches
        let mmio_enabled: u32 = read_gpio_val(MMIO_INPUT);
        let axi_io_enabled: u32 = read_gpio_val(AXI_IO_INPUT);

        // Toggle the LEDs
        if (mmio_enabled & 1) == 1 {
            write_gpio_val(0x0, MMIO_OUTPUT);
        }
        if (axi_io_enabled & 1) == 1 {
            write_gpio_val(0x1, AXI_IO_OUTPUT);
        }

        // Pause for pause_n_cycles
        pause(pause_n_cycles);
    }
}

// timer interrupt handler
pub fn timer_interrupt_handler(_regs: &coucal_rt::PicoRV32StoredRegisters) {
}

pub fn irq3_interrupt_handler(_regs: &coucal_rt::PicoRV32StoredRegisters) {
}

picorv32_interrupts!(
    0: timer_interrupt_handler,
    3: irq3_interrupt_handler
    );
